//  Copyright 2018 Viktor Akselrod
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

unit System.DelayProc;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Math,
  System.Generics.Collections,
  System.SyncObjs;

type
  /// <summary>
  ///   Execute procedure with delay.
  ///  A new call cancels the previous procedure call.
  /// </summary>
  TDelayProc = class(TObject)
  type
    TDelayData = record
      Proc    : TThreadProcedure;
      EndTime : UInt32;
      class function Make(const AProc: TThreadProcedure; const AEndTime: UInt32): TDelayData; static;
    end;

    TWorker = class(TThread)
    strict private
    var
      FItems : TDictionary<TDelayProc,TDelayData>;
      FWait  : TEvent;
      function ExecAndGetWaitTime(): UInt32;
    protected
      procedure Execute(); override;
      procedure TerminatedSet(); override;
    public
      constructor Create();
      destructor Destroy(); override;

      procedure Exec(const ADelayProc: TDelayProc; const ADelay: UInt32; const AProc: TThreadProcedure);
      procedure Cancel(const ADelayProc: TDelayProc);
    end;
  class var
    FWorker : TWorker;
    class destructor Destroy();
  public
    destructor Destroy(); override;

    procedure Execute(const ADelay: UInt32; const AProc: TThreadProcedure);
  end;

implementation

{$REGION 'TDelayProc.TDelayProcData'}
{ TDelayProc.TDelayProcData }

class function TDelayProc.TDelayData.Make(const AProc: TThreadProcedure; const AEndTime: UInt32): TDelayData;
begin
  Result.Proc := AProc;
  Result.EndTime := AEndTime;
end;
{$ENDREGION 'TDelayProc.TDelayProcData'}

{$REGION 'TDelayProc.TWorker'}
{ TDelayProc.TWorker }

constructor TDelayProc.TWorker.Create();
begin
  inherited;
  FWait := TEvent.Create(nil, False, False, String.Empty);
  FItems := TDictionary<TDelayProc,TDelayData>.Create();
end;

destructor TDelayProc.TWorker.Destroy();
begin
  inherited;
  FreeAndNil(FWait);
  FreeAndNil(FItems);
end;

procedure TDelayProc.TWorker.Execute();
begin
  while (not Terminated) do begin
    FWait.WaitFor(ExecAndGetWaitTime());
  end;
end;

procedure TDelayProc.TWorker.TerminatedSet();
begin
  inherited;
  if (FWait <> nil) then begin
    FWait.SetEvent();
  end;
end;

function TDelayProc.TWorker.ExecAndGetWaitTime(): UInt32;
var
  LItems  : TArray<TPair<TDelayProc,TDelayData>>;
  LItem   : TPair<TDelayProc,TDelayData>;
  LTick   : UInt32;
  LDelay  : Int64;
begin
  TMonitor.Enter(FItems);
  try
    if (FItems.Count = 0) then begin
      Exit(INFINITE);
    end;
    LItems := FItems.ToArray();
    Result := 0;
    LTick := TThread.GetTickCount();
    for LItem in LItems do begin
      LDelay := Int64(LItem.Value.EndTime) - Int64(LTick);
      if (LDelay <= 0) then begin
        // Remove task from list.
        FItems.Remove(LItem.Key);
        // Async exec proc.
        TThread.Queue(TThread.CurrentThread, LItem.Value.Proc);
      end else begin
        // Calculate min delay value.
        Result := Min(Result, LDelay);
      end;
    end;
    if (FItems.Count = 0) then begin
      Result := INFINITE;
    end;
  finally
    TMonitor.Exit(FItems);
  end;
end;

procedure TDelayProc.TWorker.Cancel(const ADelayProc: TDelayProc);
begin
  TMonitor.Enter(FItems);
  try
    FItems.Remove(ADelayProc);
  finally
    TMonitor.Exit(FItems);
  end;
  FWait.SetEvent();
end;

procedure TDelayProc.TWorker.Exec(const ADelayProc: TDelayProc; const ADelay: UInt32; const AProc: TThreadProcedure);
begin
  if (not Assigned(AProc)) then begin
    Exit;
  end;
  if (ADelay = 0) then begin
    AProc();
    Exit;
  end;
  TMonitor.Enter(FItems);
  try
    FItems.AddOrSetValue(ADelayProc, TDelayData.Make(AProc, TThread.GetTickCount() + ADelay));
  finally
    TMonitor.Exit(FItems);
  end;
  FWait.SetEvent();
end;
{$ENDREGION 'TDelayProc.TWorker'}

{$REGION 'TDelayProc'}
{ TDelayProc }

class destructor TDelayProc.Destroy();
begin
  FreeAndNil(FWorker);
end;

destructor TDelayProc.Destroy();
begin
  if (FWorker <> nil) then begin
    FWorker.Cancel(Self);
  end;
  inherited;
end;

procedure TDelayProc.Execute(const ADelay: UInt32; const AProc: TThreadProcedure);
begin
  if (FWorker = nil) then begin
    FWorker := TWorker.Create();
  end;
  FWorker.Exec(Self, ADelay, AProc);
end;
{$ENDREGION 'TDelayProc'}

end.

//  Copyright 2018 Viktor Akselrod
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

unit System.MultiEvent;

interface

uses
  System.SysUtils;

type
  TEventArgs = class(TObject)
  strict private
  class var
    FEmpty : TEventArgs;
    class constructor Create();
    class destructor Destroy();
  public
    class property Empty: TEventArgs read FEmpty;
  end;

  TEventArgs<T> = class(TEventArgs)
  strict private
  var
    FData : T;
  public
    constructor Create(const AData: T);

    property Data: T read FData write FData;
  end;

  TDelegate<T: TEventArgs> = reference to procedure (Sender: TObject; const E: T);

  TMultiEvent<T: TEventArgs> = record
  strict private
  var
    FItems : TArray<TDelegate<T>>;
    function IndexOf(const ADelegate: TDelegate<T>): Int32;
    function GetIsEmpty(): Boolean;
  public
    procedure RaiseEvent(Sender: TObject; const E: T);

    class operator Add(const AEvent: TMultiEvent<T>; const ADelegate: TDelegate<T>): TMultiEvent<T>;
    class operator Subtract(const AEvent: TMultiEvent<T>; const ADelegate: TDelegate<T>): TMultiEvent<T>;
    class operator Implicit(const ADelegate: TDelegate<T>): TMultiEvent<T>;

    function Contains(const ADelegate: TDelegate<T>): Boolean; inline;
    function Subscribe(const ADelegate: TDelegate<T>): TDelegate<T>;
    procedure Unsubscribe(const ADelegate: TDelegate<T>);

    property IsEmpty: Boolean read GetIsEmpty;
  end;

implementation

{ TEventArgs }

class constructor TEventArgs.Create();
begin
  FEmpty := TEventArgs.Create();
end;

class destructor TEventArgs.Destroy();
begin
  FreeAndNil(FEmpty);
end;

{ TEventArgs<T> }

constructor TEventArgs<T>.Create(const AData: T);
begin
  inherited Create();
  FData := AData;
end;

{ TMultiEvent<T> }

class operator TMultiEvent<T>.Implicit(const ADelegate: TDelegate<T>): TMultiEvent<T>;
begin
  Result.Subscribe(ADelegate);
end;

class operator TMultiEvent<T>.Add(const AEvent: TMultiEvent<T>; const ADelegate: TDelegate<T>): TMultiEvent<T>;
begin
  Result := AEvent;
  Result.Subscribe(ADelegate);
end;

class operator TMultiEvent<T>.Subtract(const AEvent: TMultiEvent<T>; const ADelegate: TDelegate<T>): TMultiEvent<T>;
begin
  Result := AEvent;
  Result.Unsubscribe(ADelegate);
end;

function TMultiEvent<T>.IndexOf(const ADelegate: TDelegate<T>): Int32;
var
  I : Int32;
begin
  for I := 0 to High(FItems) do begin
    if (TDelegate<T>(FItems[I]) = TDelegate<T>(ADelegate)) then begin
      Exit(I);
    end;
  end;
  Result := -1;
end;

function TMultiEvent<T>.Contains(const ADelegate: TDelegate<T>): Boolean;
begin
  Result := IndexOf(ADelegate) <> -1;
end;

function TMultiEvent<T>.GetIsEmpty(): Boolean;
begin
  Result := FItems = nil;
end;

function TMultiEvent<T>.Subscribe(const ADelegate: TDelegate<T>): TDelegate<T>;
{$IF CompilerVersion < 28}  // XE6 and above
var
  L : Int32;
{$ENDIF}
begin
  Result := ADelegate;
  if (not Assigned(Result)) then begin
    Exit;
  end;
  if (Contains(Result)) then begin
    Exit;
  end;
  {$IF CompilerVersion >= 28}  // XE7 and above
  Insert(TDelegate<T>(Result), FItems, Int32.MaxValue);
  {$ELSE}
  L := Length(FItems);
  SetLength(FItems, L + 1);
  FItems[L] := Result;
  {$ENDIF}
end;

procedure TMultiEvent<T>.Unsubscribe(const ADelegate: TDelegate<T>);
var
  I    : Int32;
  {$IF CompilerVersion < 28}  // XE6 and below
  L, H : Int32;
  {$ENDIF}
begin
  if (not Assigned(ADelegate)) then begin
    Exit;
  end;
  I := IndexOf(ADelegate);
  if (I = -1) then begin
    Exit;
  end;
  {$IF CompilerVersion >= 28}  // XE7 and above
  Delete(FItems, I, 1);
  {$ELSE}
  H := High(FItems);
  L := H - I;
  if (L > 0) then begin
    // Free memory for deleting element.
    Finalize(FItems[I]);
    Move(FItems[I + 1], FItems[I], L * SizeOf(FItems[0]));
    // Zero memory for last element, to prevent free it.
    Initialize(FItems[H]);
  end;
  SetLength(FItems, H);
  {$ENDIF}
end;

procedure TMultiEvent<T>.RaiseEvent(Sender: TObject; const E: T);
var
  LDelegate : TDelegate<T>;
begin
  for LDelegate in FItems do begin
    LDelegate(Sender, E);
  end;
end;

end.

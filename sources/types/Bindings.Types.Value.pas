//  Copyright 2018 Viktor Akselrod
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

unit Bindings.Types.Value;

interface

uses
  System.SysUtils,
  System.TypInfo,
  System.Rtti,
  {$IF CompilerVersion >= 27}  // XE6 and above
  System.Messaging,
  {$ELSE}
  FMX.Messages,
  {$ENDIF}
  System.MultiEvent,
  System.Generics.Defaults,
  System.Bindings.Helper,
  Bindings.Types.ValueContainer;

type
  TRecipient = (
    None,
    Self,
    Childs,
    SelfAndChilds
  );

  TCustomBindClass = class of TCustomBind;

  TCustomBind = class abstract(TObject)
  strict private
  type
    {$REGION 'Messages'}
    TUpdateMessage            = class(TMessage<Boolean>);
    TDestroyMessage           = class(TMessage);
    TResetNotificationMessage = class(TMessage);
    {$ENDREGION 'Messages'}
    TInternalState = (
      NeedNotify,
      Destroying
    );
    TInternalStates = set of TInternalState;
  var
    FOwner       : TCustomBind;
    FState       : TInternalStates;
    FUpdateCount : Int32;
    procedure SetOwner(const AValue: TCustomBind);
    procedure UpdateMessage(const Sender: TObject; const AMessage: TMessage);
    procedure DestroyMessage(const Sender: TObject; const AMessage: TMessage);
    procedure ResetNotificationMessage(const Sender: TObject; const AMessage: TMessage);
    procedure DoNotifyChange();
    function GetIsDestroying(): Boolean; inline;

    class constructor Create();
    class destructor Destroy();
  private
    function GetIsUpdating: Boolean;
  protected
  class var
    FMessageManager : TMessageManager;
    procedure NotifyChange(const AForce: Boolean);
    function GetPropertyName(): String; virtual;

    property IsDestroying: Boolean read GetIsDestroying;
  public
    constructor Create(const AOwner: TCustomBind = nil); virtual;
    destructor Destroy(); override;

    procedure AfterConstruction(); override;
    procedure BeforeDestruction(); override;

    procedure BeginUpdate();
    procedure EndUpdate(const AResetNotification: TRecipient = TRecipient.None);
    procedure ResetNotification(const ARecipient: TRecipient);
    procedure Update(const AForce: Boolean = False); virtual;

    property Owner: TCustomBind read FOwner write SetOwner;
    property IsUpdating: Boolean read GetIsUpdating;
  end;

  TCustomBindValue<T> = class abstract(TCustomBind)
  strict private
  var
    FComparer : IComparer<T>;
    FOnChange : TMultiEvent<TEventArgs>;
    procedure SetComparer(const AValue: IComparer<T>);
  protected
    function GetValue(): T; virtual; abstract;
    procedure SetValue(AValue: T); virtual; abstract;
    procedure DoClear(); virtual;
    procedure Change();
  public
    constructor Create(const AOwner: TCustomBind = nil); override;

    function ToString(): String; override;
    procedure Clear();

    property Comparer: IComparer<T> read FComparer write SetComparer;
    property Value: T read GetValue write SetValue;

    property OnChange: TMultiEvent<TEventArgs> read FOnChange write FOnChange;
  end;

  TBindValue<T> = class(TCustomBindValue<T>)
  strict private
  var
    FContainer : IValueContainer<T>;
    FChanging  : Boolean;
  protected
    function GetValue(): T; override;
    procedure SetValue(AValue: T); override;
    function GetPropertyName(): String; override;
    function PrepareNewValue(const AChanged: Boolean; var AValue: T): Boolean; virtual;
  public
    constructor Create(const AOwner: TCustomBind = nil); override;
    constructor CreateEx(const AOwner: TCustomBind; const AValue: T; const AContainer: IValueContainer<T> = nil);
  end;

  {$REGION 'aliases'}
  TBindInt8     = TBindValue<Int8>;
  TBindUInt8    = TBindValue<UInt8>;
  TBindInt16    = TBindValue<Int16>;
  TBindUInt16   = TBindValue<UInt16>;
  TBindInt32    = TBindValue<Int32>;
  TBindUInt32   = TBindValue<UInt32>;
  TBindString   = TBindValue<String>;
  TBindBoolean  = TBindValue<Boolean>;
  TBindDouble   = TBindValue<Double>;
  TBindSingle   = TBindValue<Single>;
  TBindDateTime = TBindValue<TDateTime>;
  TBindTime     = TBindValue<TTime>;
  TBindDate     = TBindValue<TDate>;
  {$ENDREGION 'aliases'}

  TValidateEventArgs<T> = class(TEventArgs)
  strict private
  var
    FNewValue : T;
    FError    : String;
    FSuccess  : Boolean;
  public
    property NewValue: T read FNewValue write FNewValue;
    property Error: String read FError write FError;
    property Success: Boolean read FSuccess write FSuccess;
  end;

  TBindValidatedValue<T> = class(TBindValue<T>)
  strict private
  var
    FNeedValidate  : Boolean;
    FEvalError     : TBindString;
    FLazyValidate  : TBindBoolean;
    FOnValidate    : TMultiEvent<TValidateEventArgs<T>>;
  protected
    function PrepareNewValue(const AChanged: Boolean; var AValue: T): Boolean; override;
    function DoValidate(var AValue: T): Boolean; virtual;
  public
    constructor Create(const AOwner: TCustomBind = nil); override;
    destructor Destroy(); override;

    property EvalError: TBindString read FEvalError;
    property LazyValidate: TBindBoolean read FLazyValidate;

    property OnValidate: TMultiEvent<TValidateEventArgs<T>> read FOnValidate write FOnValidate;
  end;

  {$REGION 'aliases'}
  TBindValidatedInt8     = TBindValidatedValue<Int8>;
  TBindValidatedUInt8    = TBindValidatedValue<UInt8>;
  TBindValidatedInt16    = TBindValidatedValue<Int16>;
  TBindValidatedUInt16   = TBindValidatedValue<UInt16>;
  TBindValidatedInt32    = TBindValidatedValue<Int32>;
  TBindValidatedUInt32   = TBindValidatedValue<UInt32>;
  TBindValidatedString   = TBindValidatedValue<String>;
  TBindValidatedBoolean  = TBindValidatedValue<Boolean>;
  TBindValidatedDouble   = TBindValidatedValue<Double>;
  TBindValidatedSingle   = TBindValidatedValue<Single>;
  TBindValidatedDateTime = TBindValidatedValue<TDateTime>;
  TBindValidatedTime     = TBindValidatedValue<TTime>;
  TBindValidatedDate     = TBindValidatedValue<TDate>;
  {$ENDREGION 'aliases'}

  TBindRange<T> = class(TBindValidatedValue<T>)
  strict private
  var
    FMinValue : TBindValidatedValue<T>;
    FMaxValue : TBindValidatedValue<T>;
    FLimitSet : TBindBoolean;
  protected
    function DoValidate(var AValue: T): Boolean; override;
  public
    constructor Create(const AOwner: TCustomBind = nil); override;
    destructor Destroy(); override;

    procedure SetRange(AMinValue, AMaxValue: T);

    property MinValue: TBindValidatedValue<T> read FMinValue;
    property MaxValue: TBindValidatedValue<T> read FMaxValue;
    property LimitSet: TBindBoolean read FLimitSet;
  end;

  {$REGION 'aliases'}
  TBindRangeInt8     = TBindRange<Int8>;
  TBindRangeUInt8    = TBindRange<UInt8>;
  TBindRangeInt16    = TBindRange<Int16>;
  TBindRangeUInt16   = TBindRange<UInt16>;
  TBindRangeInt32    = TBindRange<Int32>;
  TBindRangeUInt32   = TBindRange<UInt32>;
  TBindRangeString   = TBindRange<String>;
  TBindRangeBoolean  = TBindRange<Boolean>;
  TBindRangeDouble   = TBindRange<Double>;
  TBindRangeSingle   = TBindRange<Single>;
  TBindRangeDateTime = TBindRange<TDateTime>;
  TBindRangeTime     = TBindRange<TTime>;
  TBindRangeDate     = TBindRange<TDate>;
  {$ENDREGION 'aliases'}

implementation

{ TCustomBind }

class constructor TCustomBind.Create();
begin
  FMessageManager := TMessageManager.Create();
end;

class destructor TCustomBind.Destroy();
begin
  FreeAndNil(FMessageManager);
end;

constructor TCustomBind.Create(const AOwner: TCustomBind);
begin
  inherited Create();
  Owner := AOwner;
  FMessageManager.SubscribeToMessage(TUpdateMessage, UpdateMessage);
  FMessageManager.SubscribeToMessage(TDestroyMessage, DestroyMessage);
  FMessageManager.SubscribeToMessage(TResetNotificationMessage, ResetNotificationMessage);
end;

destructor TCustomBind.Destroy();
begin
  FMessageManager.SendMessage(Self, TDestroyMessage.Create(), True);
  FMessageManager.Unsubscribe(TResetNotificationMessage, ResetNotificationMessage);
  FMessageManager.Unsubscribe(TUpdateMessage, UpdateMessage);
  FMessageManager.Unsubscribe(TDestroyMessage, DestroyMessage);
  inherited;
end;

procedure TCustomBind.AfterConstruction();
begin
  inherited;
  ResetNotification(TRecipient.Self);
end;

procedure TCustomBind.BeforeDestruction();
begin
  inherited;
  Include(FState, TInternalState.Destroying);
end;

function TCustomBind.GetIsDestroying(): Boolean;
begin
  Result := TInternalState.Destroying in FState;
end;

function TCustomBind.GetIsUpdating: Boolean;
begin
  Result := FUpdateCount <> 0;
end;

procedure TCustomBind.BeginUpdate();
begin
  Inc(FUpdateCount);
  FMessageManager.SendMessage(Self, TUpdateMessage.Create(True), True);
end;

procedure TCustomBind.EndUpdate(const AResetNotification: TRecipient);
begin
  Dec(FUpdateCount);
  if (FUpdateCount = 0) then begin
    ResetNotification(AResetNotification);
    if (TInternalState.NeedNotify in FState) then begin
      DoNotifyChange();
    end;
  end;
  FMessageManager.SendMessage(Self, TUpdateMessage.Create(False), True);
end;

function TCustomBind.GetPropertyName(): String;
begin
  Result := String.Empty;
end;

procedure TCustomBind.NotifyChange(const AForce: Boolean);
begin
  if (FUpdateCount = 0) or (AForce) then begin
    DoNotifyChange();
  end else begin
    Include(FState, TInternalState.NeedNotify);
  end;
end;

procedure TCustomBind.DoNotifyChange();
begin
  if (IsDestroying) then begin
    Exit;
  end;
  Exclude(FState, TInternalState.NeedNotify);
  TBindings.Notify(Self, GetPropertyName());
end;

procedure TCustomBind.Update(const AForce: Boolean);
begin
  NotifyChange(AForce);
end;

procedure TCustomBind.ResetNotification(const ARecipient: TRecipient);
var
  LContext  : TRttiContext;
  LType     : TRttiType;
  LProp     : TRttiProperty;
  LValue    : TValue;
  LObj      : TCustomBind;
  LTypeInfo : PTypeInfo;
begin
  if (ARecipient in [TRecipient.Self, TRecipient.SelfAndChilds]) then begin
    Exclude(FState, TInternalState.NeedNotify);
    LType := LContext.GetType(ClassType);
    for LProp in LType.GetProperties() do begin
      LTypeInfo := LProp.PropertyType.Handle;
      if (LTypeInfo <> nil) and (LTypeInfo^.Kind = TTypeKind.tkClass) and (LTypeInfo^.TypeData^.ClassType.InheritsFrom(TCustomBind)) then begin
        if (LProp.IsReadable) then begin
          LValue := LProp.GetValue(Self);
          if (not LValue.IsEmpty) and (LValue.TryAsType<TCustomBind>(LObj)) and (LObj <> FOwner) then begin
            LObj.ResetNotification(TRecipient.SelfAndChilds);
          end;
        end;
      end;
    end;
  end;
  if (ARecipient in [TRecipient.Childs, TRecipient.SelfAndChilds]) then begin
    FMessageManager.SendMessage(Self, TResetNotificationMessage.Create(), True);
  end;
end;

procedure TCustomBind.DestroyMessage(const Sender: TObject; const AMessage: TMessage);
begin
  if (Sender <> Self) and (AMessage is TDestroyMessage) then begin
    if (Sender = FOwner) then begin
      Owner := nil;
    end;
  end;
end;

procedure TCustomBind.UpdateMessage(const Sender: TObject; const AMessage: TMessage);
begin
  if (Sender <> Self) and (AMessage is TUpdateMessage) then begin
    if (Sender = FOwner) then begin
      if (TUpdateMessage(AMessage).Value) then begin
        BeginUpdate();
      end else begin
        EndUpdate();
      end;
    end;
  end;
end;

procedure TCustomBind.ResetNotificationMessage(const Sender: TObject; const AMessage: TMessage);
begin
  if (Sender <> Self) and (AMessage is TResetNotificationMessage) then begin
    if (Sender = FOwner) then begin
      ResetNotification(TRecipient.SelfAndChilds);
    end;
  end;
end;

procedure TCustomBind.SetOwner(const AValue: TCustomBind);
var
  LUpdateDiff : Int32;
begin
  if (FOwner <> AValue) then begin
    LUpdateDiff := 0;
    if (FOwner <> nil) then begin
      Dec(LUpdateDiff, FOwner.FUpdateCount);
    end;
    FOwner := AValue;
    if (FOwner <> nil) then begin
      Inc(LUpdateDiff, FOwner.FUpdateCount);
    end;
    if (LUpdateDiff > 0) then begin
      while (LUpdateDiff > 0) do begin
        BeginUpdate();
        Dec(LUpdateDiff);
      end;
    end else begin
      while (LUpdateDiff <0) do begin
        EndUpdate();
        Inc(LUpdateDiff);
      end;
    end;
  end;
end;

{ TCustomBindValue<T> }

constructor TCustomBindValue<T>.Create(const AOwner: TCustomBind);
begin
  inherited Create(AOwner);
  Comparer := nil;
end;

procedure TCustomBindValue<T>.DoClear();
begin
  Value := Default(T);
end;

procedure TCustomBindValue<T>.Clear();
begin
  DoClear();
end;

procedure TCustomBindValue<T>.Change();
begin
  if (IsDestroying) then begin
    Exit;
  end;
  if (not FOnChange.IsEmpty) then begin
    FOnChange.RaiseEvent(Self, TEventArgs.Empty);
  end;
end;

procedure TCustomBindValue<T>.SetComparer(const AValue: IComparer<T>);
begin
  FComparer := AValue;
  if (FComparer = nil) then begin
    FComparer := TComparer<T>.Default;
  end;
end;

function TCustomBindValue<T>.ToString(): String;
var
  LValue: TValue;
begin
  LValue := TValue.From<T>(Value);
  Result := LValue.ToString();
end;

{ TBindValue<T> }

constructor TBindValue<T>.Create(const AOwner: TCustomBind);
begin
  inherited;
  if (FContainer = nil) then begin
    FContainer := TValueContainer<T>.Create(True);
  end;
end;

constructor TBindValue<T>.CreateEx(const AOwner: TCustomBind; const AValue: T;
  const AContainer: IValueContainer<T>);
begin
  FContainer := AContainer;
  Create(AOwner);
  Value := AValue;
end;

function TBindValue<T>.GetPropertyName(): String;
begin
  Result := 'Value';
end;

function TBindValue<T>.GetValue(): T;
begin
  Result := FContainer.Value;
end;

function TBindValue<T>.PrepareNewValue(const AChanged: Boolean; var AValue: T): Boolean;
begin
  Result := False;
end;

procedure TBindValue<T>.SetValue(AValue: T);
var
  LNeedNotify : Boolean;
  LChanged    : Boolean;
  LValue      : T;
begin
  if (FChanging) then begin
    Exit;
  end;
  FChanging := True;
  try
    LValue := FContainer.Value;
    LChanged := Comparer.Compare(LValue, AValue) <> 0;
    LNeedNotify := LChanged;
    if (PrepareNewValue(LChanged, AValue)) then begin
      LChanged := Comparer.Compare(LValue, AValue) <> 0;
    end;
    if (LChanged) then begin
      FContainer.Value := AValue;
      LNeedNotify := True;
    end;
    if (LNeedNotify) then begin
      NotifyChange(False);
    end;
    if (LChanged) then begin
      Change();
    end;
  finally
    FChanging := False;
  end;
end;

{ TBindValidatedValue<T> }

constructor TBindValidatedValue<T>.Create(const AOwner: TCustomBind);
begin
  inherited;
  FEvalError := TBindString.Create(Self);
  FLazyValidate := TBindBoolean.Create(Self);
end;

destructor TBindValidatedValue<T>.Destroy();
begin
  FreeAndNil(FLazyValidate);
  FreeAndNil(FEvalError);
  inherited;
end;

function TBindValidatedValue<T>.DoValidate(var AValue: T): Boolean;
var
  LEventArgs : TValidateEventArgs<T>;
begin
  if (IsDestroying) then begin
    Exit(False);
  end;
  if (not FNeedValidate) then begin
    Exit(False);
  end;
  FNeedValidate := False;
  FEvalError.Value := String.Empty;
  if (FOnValidate.IsEmpty) then begin
    Exit(False);
  end;
  LEventArgs := TValidateEventArgs<T>.Create();
  try
    LEventArgs.Success := True;
    LEventArgs.NewValue := AValue;
    FOnValidate.RaiseEvent(Self, LEventArgs);
    AValue := LEventArgs.NewValue;
    if (not LEventArgs.Success) then begin
      FEvalError.Value := LEventArgs.Error;
    end;
  finally
    FreeAndNil(LEventArgs);
  end;
  Result := True;
end;

function TBindValidatedValue<T>.PrepareNewValue(const AChanged: Boolean; var AValue: T): Boolean;
begin
  if (AChanged) then begin
    FNeedValidate := True;
  end;
  if (not FLazyValidate.Value) or (not AChanged) then begin
    Result := DoValidate(AValue);
  end else begin
    Result := False;
  end;
end;

{ TBindRange<T> }

constructor TBindRange<T>.Create(const AOwner: TCustomBind);
begin
  inherited;
  FMinValue := TBindValidatedValue<T>.Create(Self);
  FMinValue.Comparer := Comparer;
  FMinValue.OnValidate :=
    procedure(Sender: TObject; const E: TValidateEventArgs<T>)
    begin
      E.Success := True;
      if (Comparer.Compare(E.NewValue, FMaxValue.Value) > 0) then begin
        E.NewValue := FMaxValue.Value;
      end;
    end;
  FMinValue.OnChange :=
    procedure(Sender: TObject; const E: TEventArgs)
    begin
      FLimitSet.Value := True;
      if (Comparer.Compare(Value, FMinValue.Value) < 0) then begin
        Value := FMinValue.Value;
      end;
    end;
  FMaxValue := TBindValidatedValue<T>.Create(Self);
  FMaxValue.Comparer := Comparer;
  FMaxValue.OnValidate :=
    procedure(Sender: TObject; const E: TValidateEventArgs<T>)
    begin
      E.Success := True;
      if (Comparer.Compare(E.NewValue, FMinValue.Value) < 0) then begin
        E.NewValue := FMinValue.Value;
      end;
    end;
  FMaxValue.OnChange :=
    procedure(Sender: TObject; const E: TEventArgs)
    begin
      FLimitSet.Value := True;
      if (Comparer.Compare(Value, FMaxValue.Value) > 0) then begin
        Value := FMaxValue.Value;
      end;
    end;
  FLimitSet := TBindBoolean.Create(Self);
end;

destructor TBindRange<T>.Destroy();
begin
  FreeAndNil(FLimitSet);
  FreeAndNil(FMinValue);
  FreeAndNil(FMaxValue);
  inherited;
end;

function TBindRange<T>.DoValidate(var AValue: T): Boolean;
begin
  Result := inherited;
  if (not LimitSet.Value) then begin
    Exit;
  end;
  if (Comparer.Compare(AValue, FMaxValue.Value) > 0) then begin
    AValue := FMaxValue.Value;
    Result := True;
  end else if (Comparer.Compare(AValue, FMinValue.Value) < 0) then begin
    AValue := FMinValue.Value;
    Result := True;
  end;
end;

procedure TBindRange<T>.SetRange(AMinValue, AMaxValue: T);
begin
  if (Comparer.Compare(AMinValue, AMaxValue) > 0) then begin
    AMinValue := AMaxValue;
  end;
  if (Comparer.Compare(AMaxValue, FMinValue.Value) < 0) then begin
    MinValue.Value := AMinValue;
    MaxValue.Value := AMaxValue;
  end else begin
    MaxValue.Value := AMaxValue;
    MinValue.Value := AMinValue;
  end;
end;

end.

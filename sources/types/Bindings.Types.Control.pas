//  Copyright 2018 Viktor Akselrod
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

unit Bindings.Types.Control;

interface

uses
  System.SysUtils,
  System.Rtti,
  {$IF CompilerVersion >= 27}  // XE6 and above
  System.Messaging,
  {$ELSE}
  FMX.Messages,
  {$ENDIF}
  System.MultiEvent,
  System.TypInfo,
  System.Bindings.Helper,
  Bindings.Types.Value;

type
  TBindControl = class(TCustomBind)
  strict private
  type
    TEnabledMessage  = class(TMessage<Boolean>);
    TVisibleMessage  = class(TMessage<Boolean>);
  var
    FEnabled  : TBindBoolean;
    FVisible  : TBindBoolean;
    FText     : TBindString;
    FHint     : TBindString;
    procedure EnabledMessage(const Sender: TObject; const AMessage: TMessage);
    procedure VisibleMessage(const Sender: TObject; const AMessage: TMessage);
  public
    constructor Create(const AOwner: TCustomBind = nil); override;
    destructor Destroy(); override;

    property Enabled: TBindBoolean read FEnabled;
    property Visible: TBindBoolean read FVisible;
    property Text: TBindString read FText;
    property Hint: TBindString read FHint;
  end;

  TBindControl<T> = class(TBindControl)
  strict private
  var
    FData     : T;
    FBindData : TCustomBind;
    procedure SetData(const AValue: T);
  public
    constructor CreateEx(const AOwner: TCustomBind = nil);
    destructor Destroy(); override;

    property Data: T read FData write SetData;
  end;

implementation

{ TBindControl }

constructor TBindControl.Create(const AOwner: TCustomBind);
begin
  inherited;
  FEnabled := TBindBoolean.CreateEx(Self, True);
  FEnabled.OnChange :=
    procedure(Sender: TObject; const E: TEventArgs)
    begin
      FMessageManager.SendMessage(Self, TEnabledMessage.Create(FEnabled.Value));
    end;
  FVisible := TBindBoolean.CreateEx(Self, True);
  FVisible.OnChange :=
    procedure(Sender: TObject; const E: TEventArgs)
    begin
      FMessageManager.SendMessage(Self, TVisibleMessage.Create(FVisible.Value));
    end;
  FText := TBindString.Create(Self);
  FHint := TBindString.Create(Self);
  FMessageManager.SubscribeToMessage(TEnabledMessage, EnabledMessage);
  FMessageManager.SubscribeToMessage(TVisibleMessage, VisibleMessage);
end;

destructor TBindControl.Destroy();
begin
  FMessageManager.Unsubscribe(TEnabledMessage, EnabledMessage);
  FMessageManager.Unsubscribe(TVisibleMessage, VisibleMessage);
  FreeAndNil(FText);
  FreeAndNil(FHint);
  FreeAndNil(FVisible);
  FreeAndNil(FEnabled);
  inherited;
end;

procedure TBindControl.EnabledMessage(const Sender: TObject; const AMessage: TMessage);
begin
  if (Sender <> Self) and (AMessage is TEnabledMessage) then begin
    if (Sender = Owner) then begin
      Enabled.Value := TEnabledMessage(AMessage).Value;
    end;
  end;
end;

procedure TBindControl.VisibleMessage(const Sender: TObject; const AMessage: TMessage);
begin
  if (Sender <> Self) and (AMessage is TVisibleMessage) then begin
    if (Sender = Owner) then begin
      Visible.Value := TVisibleMessage(AMessage).Value;
    end;
  end;
end;

{ TBindControl<T> }

constructor TBindControl<T>.CreateEx(const AOwner: TCustomBind);
var
  LTypeInfo : PTypeInfo;
  LTypeData : PTypeData;
  LData     : TValue;
begin
  inherited Create(AOwner);
  LTypeInfo := System.TypeInfo(T);
  if (LTypeInfo <> nil) and (LTypeInfo^.Kind = TTypeKind.tkClass) then begin
    LTypeData := LTypeInfo^.TypeData;
    if (LTypeData^.ClassType.InheritsFrom(TCustomBind)) then begin
      FBindData := TCustomBindClass(LTypeData^.ClassType).Create(Self);
      LData := FBindData;
      FData := LData.AsType<T>;
    end;
  end;
end;

destructor TBindControl<T>.Destroy();
begin
  FreeAndNil(FBindData);
  inherited;
end;

procedure TBindControl<T>.SetData(const AValue: T);
begin
  FreeAndNil(FBindData);
  FData := AValue;
end;

end.

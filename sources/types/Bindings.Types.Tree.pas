//  Copyright 2018 Viktor Akselrod
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

unit Bindings.Types.Tree;

interface

uses
  System.SysUtils,
  System.Rtti,
  {$IF CompilerVersion >= 27}  // XE6 and above
  System.Messaging,
  {$ELSE}
  FMX.Messages,
  {$ENDIF}
  System.Generics.Defaults,
  System.Generics.Collections,
  System.Bindings.Helper,
  Bindings.Types.Value;

type
  TBindTreeEnumeratorItem = class sealed(TObject)
  strict private
  var
    FValue : TCustomBind;
    FLevel : Int32;
  public
    constructor Create(const AValue: TCustomBind; const ALevel: Int32);

    property Value: TCustomBind read FValue;
    property Level: Int32 read FLevel;
  end;

  TBindTree = class;

  TBindTreeEnumerator = class sealed(TEnumerator<TBindTreeEnumeratorItem>)
  strict private
  var
    FOwner      : TBindTree;
    FEnumerator : TBindTreeEnumerator;
    FCurrent    : TBindTreeEnumeratorItem;
    FLevel      : Int32;
    FIndex      : Int32;
  protected
    function DoGetCurrent(): TBindTreeEnumeratorItem; override;
    function DoMoveNext(): Boolean; override;
  public
    constructor Create(const AOwner: TBindTree; const ALevel: Int32);
  end;

  TBindTree = class(TCustomBind)
  strict private
  var
    FItems : TObjectList<TCustomBind>;
    procedure DoChildsNotify(Sender: TObject; const AItem: TCustomBind; AAction: TCollectionNotification);
  protected
    function GetPropertyName(): String; override;
  public
    constructor Create(const AOwner: TCustomBind = nil); override;
    destructor Destroy(); override;

    function GetEnumerator(): TBindTreeEnumerator;
    procedure Clear();

    property Items: TObjectList<TCustomBind> read FItems;
  end;

implementation

{$REGION 'Enumeration'}
{ TBindTreeEnumeratorItem }

constructor TBindTreeEnumeratorItem.Create(const AValue: TCustomBind;
  const ALevel: Int32);
begin
  inherited Create();
  FValue := AValue;
  FLevel := ALevel;
end;

{ TBindTreeEnumerator }

constructor TBindTreeEnumerator.Create(const AOwner: TBindTree; const ALevel: Int32);
begin
  inherited Create();
  FOwner := AOwner;
  FLevel := ALevel;
  FIndex := 0;
  ASSERT(FOwner <> nil);
end;

function TBindTreeEnumerator.DoGetCurrent(): TBindTreeEnumeratorItem;
begin
  Result := FCurrent;
end;

function TBindTreeEnumerator.DoMoveNext(): Boolean;
var
  LValue : TCustomBind;
begin
  FCurrent := nil;
  if (FEnumerator <> nil) and (FEnumerator.MoveNext()) then begin
    FCurrent := FEnumerator.Current;
  end else begin
    FEnumerator := nil;
    // Next child value by current index.
    if (FIndex >= 0) and (FIndex < FOwner.Items.Count) then begin
      LValue := FOwner.Items[FIndex];
      if (LValue is TBindTree) then begin
        // If current child is node - create new enumeration.
        FEnumerator := TBindTreeEnumerator.Create(LValue as TBindTree, FLevel + 1);
      end;
      FCurrent := TBindTreeEnumeratorItem.Create(LValue, FLevel);
      Inc(FIndex);
    end;
  end;
  Result := FCurrent <> nil;
end;
{$ENDREGION 'Enumeration'}

{ TBindTree }

constructor TBindTree.Create(const AOwner: TCustomBind);
begin
  inherited;
  FItems := TObjectList<TCustomBind>.Create();
  FItems.OnNotify := DoChildsNotify;
end;

destructor TBindTree.Destroy();
begin
  Clear();
  FreeAndNil(FItems);
  inherited;
end;

function TBindTree.GetPropertyName(): String;
begin
  Result := 'Items';
end;

function TBindTree.GetEnumerator(): TBindTreeEnumerator;
begin
  Result := TBindTreeEnumerator.Create(Self, 0);
end;

procedure TBindTree.Clear();
begin
  BeginUpdate();
  try
    FItems.Clear();
  finally
    EndUpdate();
  end;
end;

procedure TBindTree.DoChildsNotify(Sender: TObject; const AItem: TCustomBind;
  AAction: TCollectionNotification);
begin
  case AAction of
    cnAdded: begin
      AItem.Owner := Self;
    end;
    //cnRemoved, no need do some changes. object will be destroyed.
    cnExtracted: begin
      if (AItem.Owner = Self) then begin
        AItem.Owner := nil;
      end;
    end;
  end;
  Update();
end;

end.

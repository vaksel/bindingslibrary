//  Copyright 2018 Viktor Akselrod
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

unit Bindings.Types.ValueContainer;

interface

uses
  System.TypInfo;

type
  IValueContainer<T> = interface
    ['{EB1F4B6A-2DAF-4DC2-9960-8C814CCFC98F}']
    function GetValue(): T;
    procedure SetValue(const AValue: T);

    property Value: T read GetValue write SetValue;
  end;

  TValueContainer<T> = class(TInterfacedObject, IValueContainer<T>)
  strict private
  var
    FValue    : T;
    FOwnValue : Boolean;
  protected
    function GetValue(): T;
    procedure SetValue(const AValue: T);
  public
    constructor Create(const AOwnValue: Boolean = True);
  end;

  TGetValueProc<T> = reference to function(): T;
  TSetValueProc<T> = reference to procedure (const AValue: T);

  TLinkValueContainer<T> = class(TInterfacedObject, IValueContainer<T>)
  strict private
  var
    FOnGetValue : TGetValueProc<T>;
    FOnSetValue : TSetValueProc<T>;
  protected
    function GetValue(): T;
    procedure SetValue(const AValue: T);
  public
    constructor Create(const AOnGetValue: TGetValueProc<T>; const AOnSetValue: TSetValueProc<T>);
  end;

implementation

{ TValueContainer<T> }

constructor TValueContainer<T>.Create(const AOwnValue: Boolean);
var
  LTypeInfo : PTypeInfo;
begin
  inherited Create();
  LTypeInfo := System.TypeInfo(T);
  FOwnValue := (AOwnValue) and (LTypeInfo <> nil) and (LTypeInfo^.Kind = TTypeKind.tkClass);
end;

function TValueContainer<T>.GetValue(): T;
begin
  Result := FValue;
end;

procedure TValueContainer<T>.SetValue(const AValue: T);
type
  PObject = ^TObject;
begin
  if (FOwnValue) then begin
    PObject(@FValue)^.DisposeOf();
  end;
  FValue := AValue;
end;

{ TLinkValueContainer<T> }

constructor TLinkValueContainer<T>.Create(const AOnGetValue: TGetValueProc<T>; const AOnSetValue: TSetValueProc<T>);
begin
  inherited Create();
  FOnGetValue := AOnGetValue;
  FOnSetValue := AOnSetValue;
end;

function TLinkValueContainer<T>.GetValue(): T;
begin
  if (Assigned(FOnGetValue)) then begin
    Result := FOnGetValue();
  end else begin
    Result := Default(T);
  end;
end;

procedure TLinkValueContainer<T>.SetValue(const AValue: T);
begin
  if (Assigned(FOnSetValue)) then begin
    FOnSetValue(AValue);
  end;
end;

end.

//  Copyright 2018 Viktor Akselrod
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

unit Bindings.Types.Command;

interface

uses
  System.SysUtils,
  System.Generics.Collections,
  Bindings.Types.Value;

type
  TBindCommand = class(TCustomBind)
  strict private
  var
    FProc : TProc;
    procedure SetExec(const AValue: Boolean);
  protected
    function CanExec(const ANeedExec: Boolean): Boolean; virtual;
  public
    constructor Create(const AOwner: TCustomBind = nil); override;
    constructor CreateEx(const AOwner: TCustomBind; const AProc: TProc); overload;

    procedure Run();

    property Proc: TProc read FProc write FProc;
    property Exec: Boolean write SetExec;
  end;

implementation

{ TBindCommand }

constructor TBindCommand.Create(const AOwner: TCustomBind);
begin
  inherited;
end;

constructor TBindCommand.CreateEx(const AOwner: TCustomBind; const AProc: TProc);
begin
  Create(AOwner);
  FProc := AProc;
end;

function TBindCommand.CanExec(const ANeedExec: Boolean): Boolean;
begin
  Result := (ANeedExec) and (Assigned(FProc));
end;

procedure TBindCommand.SetExec(const AValue: Boolean);
begin
  if (CanExec(AValue)) then begin
    FProc();
  end;
end;

procedure TBindCommand.Run();
begin
  Exec := True;
end;

end.

//  Copyright 2018 Viktor Akselrod
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

unit Bindings.Types.List;

interface

uses
  System.SysUtils,
  System.TypInfo,
  System.MultiEvent,
  System.Generics.Collections,
  Bindings.Types.Value;

type
  TBindList<T> = class(TCustomBindValue<T>)
  strict private
  var
    FItemIndex  : TBindValidatedInt32;
    FItems      : TList<T>;
    FOwnItems   : Boolean;
    FBindItems  : Boolean;
    procedure ItemsChanged(Sender: TObject; const AItem: T; AAction: TCollectionNotification);
    function GetCount(): Int32; inline;
    function GetHasValue(): Boolean;
  protected
    procedure SetValue(AValue: T); override;
    function GetValue(): T; override;
    function GetPropertyName(): String; override;
    procedure DoClear(); override;
  public
    constructor Create(const AOwner: TCustomBind = nil); override;
    constructor CreateEx(const AOwner: TCustomBind; const AOwnItems: Boolean);
    destructor Destroy(); override;

    function GetEnumerator(): TEnumerator<T>; inline;
    function IndexOf(const AValue: T): Int32; inline;
    function CheckIndex(const AIndex: Int32): Boolean;
    procedure Update(const AForce: Boolean = False); override;

    property Items: TList<T> read FItems;
    property Count: Int32 read GetCount;
    property ItemIndex: TBindValidatedInt32 read FItemIndex;
    property HasValue: Boolean read GetHasValue;
  end;

  {$REGION 'aliases'}
  TBindListInt8     = TBindList<Int8>;
  TBindListUInt8    = TBindList<UInt8>;
  TBindListInt16    = TBindList<Int16>;
  TBindListUInt16   = TBindList<UInt16>;
  TBindListInt32    = TBindList<Int32>;
  TBindListUInt32   = TBindList<UInt32>;
  TBindListString   = TBindList<String>;
  TBindListBoolean  = TBindList<Boolean>;
  TBindListDouble   = TBindList<Double>;
  TBindListSingle   = TBindList<Single>;
  TBindListDateTime = TBindList<TDateTime>;
  TBindListTime     = TBindList<TTime>;
  TBindListDate     = TBindList<TDate>;
  {$ENDREGION 'aliases'}

  TBindListItem<T> = class(TObject)
  strict private
  var
    FValue  : T;
    FText   : String;
  public
    constructor Create(const AText: String; const AValue: T);

    property Value: T read FValue write FValue;
    property Text: String read FText write FText;
  end;

implementation

{ TBindList<T> }

constructor TBindList<T>.Create(const AOwner: TCustomBind);
var
  LTypeInfo : PTypeInfo;
begin
  inherited;
  FItemIndex := TBindValidatedInt32.CreateEx(Self, -1);
  FItemIndex.OnChange :=
    procedure(Sender: TObject; const E: TEventArgs)
    begin
      Change();
    end;
  FItems := TList<T>.Create();
  FItems.OnNotify := ItemsChanged;
  LTypeInfo := System.TypeInfo(T);
  FOwnItems := (LTypeInfo <> nil) and (LTypeInfo^.Kind = TTypeKind.tkClass);
  FBindItems := (FOwnItems) and (LTypeInfo^.TypeData^.ClassType.InheritsFrom(TCustomBind));
end;

constructor TBindList<T>.CreateEx(const AOwner: TCustomBind; const AOwnItems: Boolean);
begin
  Create(AOwner);
  FOwnItems := FOwnItems and AOwnItems;
end;

destructor TBindList<T>.Destroy();
begin
  FreeAndNil(FItems);
  FreeAndNil(FItemIndex);
  inherited;
end;

procedure TBindList<T>.DoClear();
begin
  BeginUpdate();
  try
    FItemIndex.Value := -1;
    FItems.Clear();
  finally
    EndUpdate();
  end;
end;

function TBindList<T>.CheckIndex(const AIndex: Int32): Boolean;
begin
  Result := (AIndex >= 0) and (AIndex < Count);
end;

function TBindList<T>.GetCount(): Int32;
begin
  Result := FItems.Count;
end;

function TBindList<T>.GetEnumerator(): TEnumerator<T>;
begin
  Result := FItems.GetEnumerator();
end;

function TBindList<T>.GetHasValue(): Boolean;
begin
  Result := CheckIndex(FItemIndex.Value);
end;

function TBindList<T>.GetPropertyName(): String;
begin
  Result := 'Items';
end;

function TBindList<T>.GetValue(): T;
begin
  if (HasValue) then begin
    Result := FItems[ItemIndex.Value];
  end else begin
    Result := Default(T);
  end;
end;

function TBindList<T>.IndexOf(const AValue: T): Int32;
begin
  Result := FItems.IndexOf(AValue);
end;

procedure TBindList<T>.ItemsChanged(Sender: TObject; const AItem: T; AAction: TCollectionNotification);
type
  PCustomBind = ^TCustomBind;
begin
  case AAction of
    cnAdded: begin
      if (FBindItems) then begin
        PCustomBind(@AItem)^.Owner := Self;
      end;
    end;
    cnRemoved: begin
      if (FOwnItems) then begin
        PObject(@AItem)^.DisposeOf();
      end;
    end;
    cnExtracted: begin
      if (FBindItems) then begin
        if (PCustomBind(@AItem)^.Owner = Self) then begin
          PCustomBind(@AItem)^.Owner := nil;
        end;
      end;
    end;
  end;
  Update();
end;

procedure TBindList<T>.SetValue(AValue: T);
begin
  FItemIndex.Value := IndexOf(AValue);
end;

procedure TBindList<T>.Update(const AForce: Boolean);
begin
  inherited;
  FItemIndex.NotifyChange(AForce);
end;

{ TBindListItem<T> }

constructor TBindListItem<T>.Create(const AText: String; const AValue: T);
begin
  inherited Create();
  FText := AText;
  FValue := AValue;
end;

end.

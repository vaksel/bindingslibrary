//  Copyright 2018 Viktor Akselrod
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

unit Bindings.Notifications.Handler.VCL;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Bindings.Helper,
  Vcl.Controls,
  Vcl.StdCtrls,
  Vcl.ComCtrls,
  Vcl.ExtCtrls,
  Vcl.CheckLst,
  Vcl.Grids,
  Vcl.Mask,
  Vcl.Menus,
  Bindings.Notifications.Handler;

type
  TControlNotificationsVCL = class(TControlNotifications)
  protected
    procedure DoGetNotifyProperty(const ASource: TComponent; var APropertyName: String); override;
    procedure DoActivateClick(const AHandle: TClickNotificationHandle); override;
    procedure DoDeactivateClick(const AHandle: TClickNotificationHandle); override;
  end;

  TBindingsControlNotifications = class(TComponent)
  end;

implementation

type
  TOpenControl = class(TControl)
  public
    property OnClick;
    property OnDblClick;
  end;

{ TControlNotificationsVCL }

procedure TControlNotificationsVCL.DoActivateClick(const AHandle: TClickNotificationHandle);
var
  LControl  : TOpenControl;
  LMenuItem : TMenuItem;
begin
  inherited;
  if (AHandle.Source is TControl) then begin
    LControl := TOpenControl(AHandle.Source);
    AHandle.Observer.StoreOnClick := LControl.OnClick;
    AHandle.Observer.StoreOnDoubleClick := LControl.OnDblClick;
    LControl.OnClick := AHandle.Observer.OnClick;
    LControl.OnDblClick := AHandle.Observer.OnDoubleClick;
  end else if (AHandle.Source is TMenuItem) then begin
    LMenuItem := TMenuItem(AHandle.Source);
    AHandle.Observer.StoreOnClick := LMenuItem.OnClick;
    LMenuItem.OnClick := AHandle.Observer.OnClick;
    // DoubleClick is not supported.
  end;
end;

procedure TControlNotificationsVCL.DoDeactivateClick(const AHandle: TClickNotificationHandle);
var
  LControl : TOpenControl;
begin
  inherited;
  if (AHandle.Source is TControl) then begin
    LControl := TOpenControl(AHandle.Source);
    LControl.OnClick := AHandle.Observer.StoreOnClick;
    LControl.OnDblClick := AHandle.Observer.StoreOnDoubleClick;
  end;
end;

procedure TControlNotificationsVCL.DoGetNotifyProperty(const ASource: TComponent;
  var APropertyName: String);
begin
  inherited;
  if (ASource is TEdit) or (ASource is TMemo) or (ASource is TMaskEdit) or (ASource is TLabeledEdit) or (ASource is TButtonedEdit) or (ASource is TRichEdit) then begin
    APropertyName := 'Text';
  end else if (ASource is TCheckBox) or (ASource is TRadioButton) then begin
    APropertyName := 'Checked';
  end else if (ASource is TListBox) or (ASource is TComboBox) or (ASource is TRadioGroup) or (ASource is TCheckListBox) or (ASource is TComboBoxEx) then begin
    APropertyName := 'ItemIndex';
  end else if (ASource is TScrollBar) or (ASource is TTrackBar) or (ASource is TProgressBar) or (ASource is TUpDown) then begin
    APropertyName := 'Position';
  end else if (ASource is TColorBox) or (ASource is TColorListBox) then begin
    APropertyName := 'Color';
  end else if (ASource is TTabControl) or (ASource is TPageControl) then begin
    APropertyName := 'TabIndex';
  end else if (ASource is TDateTimePicker) then begin
    APropertyName := 'DateTime';
  end else if (ASource is TMonthCalendar) then begin
    APropertyName := 'Date';
  end else if (ASource is THotKey) then begin
    APropertyName := 'HotKey';
  end;
end;

initialization
  TControlNotificationsVCL.FCurrent := TControlNotificationsVCL.Create();

end.

//  Copyright 2018 Viktor Akselrod
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

unit Bindings.Notifications.Handler.FMX;

interface

uses
  System.SysUtils,
  System.Classes,
  {$IF CompilerVersion >= 27}  // XE6 and above
  System.Messaging,
  {$ELSE}
  FMX.Messages,
  {$ENDIF}
  System.Bindings.Helper,
  FMX.Controls,
  FMX.StdCtrls,
  FMX.ListBox,
  FMX.ListView,
  FMX.DateTimeCtrls,
  Bindings.Notifications.Handler;

type
  TControlNotificationsFMX = class(TControlNotifications)
  strict private
  class var
    FRadioButtonMsg : Int32;
  protected
    procedure DoGetNotifyProperty(const ASource: TComponent; var APropertyName: String); override;
    procedure DoActivateClick(const AHandle: TClickNotificationHandle); override;
    procedure DoDeactivateClick(const AHandle: TClickNotificationHandle); override;
  public
    constructor Create();
    destructor Destroy(); override;
  end;

  TBindingsControlNotifications = class(TComponent)
  end;

implementation

{ TControlNotificationsFMX }

constructor TControlNotificationsFMX.Create();
begin
  inherited;
  if (FRadioButtonMsg = 0) then begin
    FRadioButtonMsg := TMessageManager.DefaultManager.SubscribeToMessage(TRadioButtonGroupMessage,
      procedure(const Sender: TObject; const M: TMessage)
      begin
        TBindings.Notify(Sender, 'IsChecked');
      end);
  end;
end;

destructor TControlNotificationsFMX.Destroy();
begin
  if (FRadioButtonMsg <> 0) then begin
    TMessageManager.DefaultManager.Unsubscribe(TRadioButtonGroupMessage, FRadioButtonMsg);
  end;
  inherited;
end;

procedure TControlNotificationsFMX.DoActivateClick(const AHandle: TClickNotificationHandle);
var
  LControl : TControl;
begin
  inherited;
  if (AHandle.Source is TControl) then begin
    LControl := TControl(AHandle.Source);
    AHandle.Observer.StoreOnClick := LControl.OnClick;
    AHandle.Observer.StoreOnDoubleClick := LControl.OnDblClick;
    LControl.OnClick := AHandle.Observer.OnClick;
    LControl.OnDblClick := AHandle.Observer.OnDoubleClick;
  end;
end;

procedure TControlNotificationsFMX.DoDeactivateClick(const AHandle: TClickNotificationHandle);
var
  LControl : TControl;
begin
  inherited;
  if (AHandle.Source is TControl) then begin
    LControl := TControl(AHandle.Source);
    LControl.OnClick := AHandle.Observer.StoreOnClick;
    LControl.OnDblClick := AHandle.Observer.StoreOnDoubleClick;
  end;
end;

procedure TControlNotificationsFMX.DoGetNotifyProperty(const ASource: TComponent;
  var APropertyName: String);
begin
  inherited;
  if (ASource is TRadioButton) then begin
    APropertyName := 'IsChecked';
  end else if (ASource is TListBox) or (ASource is TComboBox) or (ASource is TListView) then begin
    APropertyName := 'ItemIndex';
  end else if (ASource is TTimeEdit) {$IF CompilerVersion >= 27} or (ASource is TDateEdit){$ENDIF} then begin // XE6 and above
    APropertyName := 'DateTime';
  end
end;

initialization
  TControlNotificationsFMX.FCurrent := TControlNotificationsFMX.Create();

end.

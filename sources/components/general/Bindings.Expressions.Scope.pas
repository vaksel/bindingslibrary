//  Copyright 2018 Viktor Akselrod
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

unit Bindings.Expressions.Scope;

interface

uses
  System.SysUtils,
  System.Rtti,
  System.Bindings.EvalProtocol,
  System.Bindings.EvalSys,
  Data.Bind.Components;

type
  TBindingsScope = class(TCustomBindScope, IScopeRecordEnumerable)
  strict private
    function GetMemberByName(const ASource: TObject; const AMemberName: String): TObject;
    { IScopeRecordEnumerable }
    function GetEnumerator(const AMemberName: String; ABufferCount: Integer): IScopeRecordEnumerator;
  protected
    function GetMember(const AMemberName: String): TObject; override;
  published
    property Component;
    property AutoActivate;
    property ScopeMappings;
  end;

implementation

{ TBindingsScope }

function TBindingsScope.GetEnumerator(const AMemberName: String; ABufferCount: Integer): IScopeRecordEnumerator;
var
  LMember   : TObject;
  LContext  : TRttiContext;
  LRttiType : TRttiType;
  LField    : TRttiField;
  LValue    : TValue;
begin
  LMember := GetMember(AMemberName);
  if (LMember = nil) then begin
    Exit(inherited);
  end;
  // Changing original source object to member object, get enumerator and revert again.
  LRttiType := LContext.GetType(Self.ClassType);
  if (LRttiType = nil) then begin
    Exit(nil);
  end;
  LField := LRttiType.GetField('FDataObject');
  if (LField = nil) then begin
    Exit(nil);
  end;
  LValue := LField.GetValue(Self);
  try
    LField.SetValue(Self, LMember);
    Result := inherited;
  finally
    LField.SetValue(Self, LValue);
  end;
end;

function TBindingsScope.GetMember(const AMemberName: String): TObject;
var
  LMember     : TObject;
  LMemberName : String;
begin
  if (AMemberName.IsEmpty) then begin
    Exit(nil);
  end;
  // Getting source object.
  if (Component <> nil) then begin
    LMember := Component;
  end else if (DataObject <> nil) then begin
    LMember := DataObject;
  end else begin
    Exit(nil);
  end;
  // Get source object by path.
  for LMemberName in AMemberName.Split(['.']) do begin
    LMember := GetMemberByName(LMember, LMemberName);
    if (LMember = nil) then begin
      Exit(nil);
    end;
  end;
  Result := LMember;
end;

function TBindingsScope.GetMemberByName(const ASource: TObject; const AMemberName: String): TObject;
var
  LContext  : TRttiContext;
  LRttiType : TRttiType;
  LProperty : TRttiProperty;
  LValue    : TValue;
begin
  if (AMemberName.IsEmpty) then begin
    Exit(nil);
  end;
  // Getting member object by name from source object.
  LRttiType := LContext.GetType(ASource.ClassType);
  if (LRttiType = nil) then begin
    Exit(nil);
  end;
  LProperty := LRttiType.GetProperty(AMemberName);
  if (LProperty = nil) then begin
    Exit(nil);
  end;
  LValue := LProperty.GetValue(ASource);
  if {$IF CompilerVersion < 31}(not LValue.IsObject) or (LValue.AsObject = nil){$ELSE}(not LValue.IsObjectInstance){$ENDIF} then begin
    Exit(nil);
  end;
  Result := LValue.AsObject;
end;

end.

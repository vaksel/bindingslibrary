//  Copyright 2018 Viktor Akselrod
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

unit Bindings.Notifications.Handler;

interface

uses
  System.SysUtils,
  System.Classes,
  Data.Bind.Components,
  Bindings.Notifications.Observers;

type
  TGetPropertyNameEvent = procedure (Sender: TObject; const ASource: TComponent; var APropertyName: String) of object;

  TCustomNotificationHandle = class abstract(TComponent)
  private
    FSource: TComponent;
    procedure SetSource(const Value: TComponent);
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  public
    property Source: TComponent read FSource write SetSource;
  end;

  TChangeNotificationHandle = class(TCustomNotificationHandle)
  strict private
  var
    FObserver   : IObserver;
    FObserverId : Int32;
  public
    property ObserverId: Int32 read FObserverId write FObserverId;
    property Observer: IObserver read FObserver write FObserver;
  end;

  TClickNotificationHandle = class(TCustomNotificationHandle)
  private
    FObserver : TBindingsClickObserver;
    procedure SetObserver(const Value: TBindingsClickObserver);
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  public
    property Observer: TBindingsClickObserver read FObserver write SetObserver;
  end;

  TControlNotifications = class abstract(TObject)
  strict private
  var
    FOnGetPropertyName : TGetPropertyNameEvent;
    class destructor Destroy();
    class function GetCurrent(): TControlNotifications; static;
  protected
  class var
    FCurrent : TControlNotifications;
    procedure DoActivateChange(const AHandle: TChangeNotificationHandle; const APropertyName: String;
      const ATrack: Boolean; const ATrackDelay: UInt32); virtual;
    procedure DoDeactivateChange(const AHandle: TChangeNotificationHandle); virtual;
    procedure DoActivateClick(const AHandle: TClickNotificationHandle); virtual; abstract;
    procedure DoDeactivateClick(const AHandle: TClickNotificationHandle); virtual; abstract;
    procedure DoGetNotifyProperty(const ASource: TComponent; var APropertyName: String); virtual;
  public
    function GetNotifyProperty(const ASource: TComponent): String;
    function ActivateChange(const ASource: TComponent; const APropertyName: String;
      const ATrack: Boolean; const ATrackDelay: UInt32): TChangeNotificationHandle;
    procedure DeactivateChange(var AHandle: TChangeNotificationHandle);
    function ActivateClick(const ASource: TComponent): TClickNotificationHandle;
    procedure DeactivateClick(var AHandle: TClickNotificationHandle);

    class property Current: TControlNotifications read GetCurrent;

    property OnGetPropertyName: TGetPropertyNameEvent read FOnGetPropertyName write FOnGetPropertyName;
  end;

implementation

{ TCustomNotificationHandle }

procedure TCustomNotificationHandle.Notification(AComponent: TComponent; Operation: TOperation);
begin
  if (Operation = TOperation.opRemove) and (AComponent = FSource) then
    FSource := nil;
  inherited;
end;

procedure TCustomNotificationHandle.SetSource(const Value: TComponent);
begin
  if FSource <> nil then
    FSource.RemoveFreeNotification(Self);
  FSource := Value;
  if FSource <> nil then
    FSource.FreeNotification(Self);
end;

{ TClickNotificationHandle }

procedure TClickNotificationHandle.Notification(AComponent: TComponent; Operation: TOperation);
begin
  if (Operation = TOperation.opRemove) and (AComponent = FObserver) then
    FObserver := nil;
  inherited;
end;

procedure TClickNotificationHandle.SetObserver(const Value: TBindingsClickObserver);
begin
  if FObserver <> nil then
    FObserver.RemoveFreeNotification(Self);
  FObserver := Value;
  if FObserver <> nil then
    FObserver.FreeNotification(Self);
end;

{ TControlNotifications }

class destructor TControlNotifications.Destroy();
begin
  FreeAndNil(FCurrent);
end;

class function TControlNotifications.GetCurrent(): TControlNotifications;
begin
  if (FCurrent = nil) then begin
    raise Exception.Create('Please, add a TBindingsControlNotifications first.');
  end;
  Result := FCurrent;
end;

function TControlNotifications.ActivateChange(const ASource: TComponent; const APropertyName: String;
  const ATrack: Boolean; const ATrackDelay: UInt32): TChangeNotificationHandle;
begin
  Result := TChangeNotificationHandle.Create(nil);
  Result.Source := ASource;
  DoActivateChange(Result, APropertyName, ATrack, ATrackDelay);
end;

function TControlNotifications.ActivateClick(const ASource: TComponent): TClickNotificationHandle;
begin
  Result := TClickNotificationHandle.Create(nil);
  Result.Source := ASource;
  Result.Observer := ASource.FindComponent(TBindingsClickObserver.ClassName) as TBindingsClickObserver;
  if (Result.Observer = nil) then begin
    Result.Observer := TBindingsClickObserver.Create(ASource);
    Result.Observer.Name := TBindingsClickObserver.ClassName;
    DoActivateClick(Result);
  end;
  Result.Observer.Attach();
end;

procedure TControlNotifications.DeactivateChange(var AHandle: TChangeNotificationHandle);
begin
  if (AHandle = nil) or (AHandle.Source = nil) or (AHandle.Observer = nil) then
    Exit;
  try
    DoDeactivateChange(AHandle);
  finally
    FreeAndNil(AHandle);
  end;
end;

procedure TControlNotifications.DeactivateClick(var AHandle: TClickNotificationHandle);
begin
  if (AHandle = nil) or (AHandle.Source = nil) or (AHandle.Observer = nil) then
    Exit;
  try
    AHandle.Observer.Detach();
    if not AHandle.Observer.IsAttached then
    begin
      DoDeactivateClick(AHandle);
      AHandle.Observer.Free;
    end;
  finally
    FreeAndNil(AHandle);
  end;
end;

procedure TControlNotifications.DoActivateChange(const AHandle: TChangeNotificationHandle;
  const APropertyName: String; const ATrack: Boolean; const ATrackDelay: UInt32);
begin
  if (AHandle.Source.Observers.CanObserve(TObserverMapping.ControlValueID)) then begin
    AHandle.ObserverId := TObserverMapping.ControlValueID;
    AHandle.Observer := TBindingControlValueObserver.Create(AHandle.Source, APropertyName, ATrack, ATrackDelay);
  end else if (AHandle.Source.Observers.CanObserve(TObserverMapping.PositionLinkID)) then begin
    AHandle.ObserverId := TObserverMapping.PositionLinkID;
    AHandle.Observer := TBindingPositionLinkObserver.Create(AHandle.Source, APropertyName, ATrack, ATrackDelay);
  end;
end;

procedure TControlNotifications.DoDeactivateChange(const AHandle: TChangeNotificationHandle);
begin
  AHandle.Source.Observers.RemoveObserver(AHandle.ObserverId, AHandle.Observer);
end;

procedure TControlNotifications.DoGetNotifyProperty(const ASource: TComponent; var APropertyName: String);
begin
  // nop.
end;

function TControlNotifications.GetNotifyProperty(const ASource: TComponent): String;
begin
  if (ASource = nil) then begin
    Exit(String.Empty);
  end;
  if (not GetControlValuePropertyName(ASource, Result)) then begin
    Result := String.Empty;
  end;
  DoGetNotifyProperty(ASource, Result);
  if (Assigned(FOnGetPropertyName)) then begin
    FOnGetPropertyName(Self, ASource, Result);
  end;
end;

end.

//  Copyright 2018 Viktor Akselrod
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

unit Bindings.Expressions.Designer;

interface

uses
  System.SysUtils,
  System.Classes,
  Data.Bind.Components,
  Bindings.Expressions.Components;

type
  TCustomBindDesigner = class(TInterfacedObject, IBindCompDesigner)
  protected
    function CanBindComponent(ADataBindingClass: TContainedBindCompClass; AComponent: TComponent; ADesigner: IInterface): Boolean; virtual;
    function BindsComponent(ABindComp: TContainedBindComponent; AComponent: TComponent): Boolean; virtual;
    function BindsComponentPropertyName(ABindComp: TContainedBindComponent; const APropertyName: String): Boolean; virtual;
    function GetDescription(ABindComp: TContainedBindComponent): String; virtual;
    function IsReadOnly(ABindComp: TContainedBindComponent; AExpression: TBindCompDesignExpression): Boolean; overload; virtual;
    function IsReadOnly(ABindComp: TContainedBindComponent; AItem: TCollectionItem): Boolean; overload; virtual;
    function IsReadOnly(ABindComp: TContainedBindComponent; ACollection: TCollection): Boolean; overload; virtual;
    function GetExpressions(ABindComp: TContainedBindComponent; out ATypes: TBindCompDesignerExpressionTypes): TArray<TBindCompDesignExpression>; virtual;
    function GetExpressionCollections(ABindComp: TContainedBindComponent): TArray<TBindCompDesignExpressionCollection>; virtual;
  end;

  TBindingControlDesigner = class(TCustomBindDesigner)
  protected
    function GetDescription(ABindComp: TContainedBindComponent): String; override;
  end;

  TBindingCommandDesigner = class(TCustomBindDesigner)
  protected
    function GetDescription(ABindComp: TContainedBindComponent): String; override;
  end;

implementation

{ TCustomBindDesigner }

function TCustomBindDesigner.BindsComponent(ABindComp: TContainedBindComponent; AComponent: TComponent): Boolean;
begin
  ASSERT(AComponent <> nil);
  Result := ABindComp.ControlComponent = AComponent;
end;

function TCustomBindDesigner.BindsComponentPropertyName(ABindComp: TContainedBindComponent;
  const APropertyName: String): Boolean;
begin
  Result := APropertyName = 'ControlComponent';
end;

function TCustomBindDesigner.CanBindComponent(ADataBindingClass: TContainedBindCompClass;
  AComponent: TComponent; ADesigner: IInterface): Boolean;
begin
  Result := True;
end;

function TCustomBindDesigner.GetDescription(ABindComp: TContainedBindComponent): String;
begin
  Result := String.Empty;
end;

function TCustomBindDesigner.GetExpressionCollections(ABindComp: TContainedBindComponent): TArray<TBindCompDesignExpressionCollection>;
begin
  Result := nil;
end;

function TCustomBindDesigner.GetExpressions(ABindComp: TContainedBindComponent;
  out ATypes: TBindCompDesignerExpressionTypes): TArray<TBindCompDesignExpression>;
begin
  Result := nil;
end;

function TCustomBindDesigner.IsReadOnly(ABindComp: TContainedBindComponent;
  AExpression: TBindCompDesignExpression): Boolean;
begin
  Result := False;
end;

function TCustomBindDesigner.IsReadOnly(ABindComp: TContainedBindComponent;
  AItem: TCollectionItem): Boolean;
begin
  Result := False;
end;

function TCustomBindDesigner.IsReadOnly(ABindComp: TContainedBindComponent;
  ACollection: TCollection): Boolean;
begin
  Result := False;
end;

{ TBindingControlDesigner }

function TBindingControlDesigner.GetDescription(ABindComp: TContainedBindComponent): String;
var
  LBinding    : TBindingControl;
  LControl    : String;
  LSource     : String;
  LDirection: string;
begin
  if (ABindComp is TBindingControl) then begin
    LBinding := ABindComp as TBindingControl;
    if LBinding.BindingMode = TBindingMode.TwoWay then
      LDirection := '<->'
    else
      LDirection := '->';
    if (LBinding.ControlComponent <> nil) then begin
      LControl := LBinding.ControlComponent.Name;
      if not LBinding.ControlExpression.IsEmpty then
        LControl := LControl + '.' + LBinding.ControlExpression;
    end;
    if (LBinding.SourceComponent <> nil) then begin
      LSource := LBinding.SourceComponent.Name;
      if not LBinding.SourceMemberName.IsEmpty then
        LSource := LSource + '.' + LBinding.SourceMemberName;
      if not LBinding.SourceExpression.IsEmpty then
        LSource := LSource + '.' + LBinding.SourceExpression;
    end;
  end;
  Result := String.Format('"%s" %s "%s"', [LSource, LDirection, LControl]);
end;

{ TBindingCommandDesigner }

function TBindingCommandDesigner.GetDescription(ABindComp: TContainedBindComponent): String;
var
  LBinding    : TBindingCommand;
  LControl    : String;
  LSource     : String;
  LSourceCmd  : String;
begin
  if (ABindComp is TBindingCommand) then begin
    LBinding := ABindComp as TBindingCommand;
    if (LBinding.ControlComponent <> nil) then begin
      LControl := LBinding.ControlComponent.Name;
    end;
    if (LBinding.SourceComponent <> nil) then begin
      LSource := LBinding.SourceComponent.Name;
      LSourceCmd := LBinding.SourceCommand;
    end;
  end;
  Result := String.Format('Bind control "%s" to command "%s.%s"', [LControl, LSource, LSourceCmd]);
end;

end.

//  Copyright 2018-2019 Viktor Akselrod
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
unit Bindings.ViewModelAdapter;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Rtti,
  System.Bindings.EvalProtocol,
  System.Bindings.EvalSys,
  System.Bindings.ObjEval,
  Data.Bind.Components,
  Bindings.ViewModel;

type
  TAutoOption = (
    Create,
    Destroy,
    Activate
  );
  TAutoOptions = set of TAutoOption;

  TViewModelAdapter = class(TBaseBindScopeComponent, IScopeActive, IScopeRecordEnumerable)
  strict private
  type
    TScopeRecordEnumerator = class(TInterfacedObject, IScopeRecordEnumerator)
    strict private
    var
      FObject       : TObject;
      FEnumerator   : TObject;
      FMoveNext     : TRttiMethod;
      FParentScope  : IScope;
      procedure First();
      function GetCurrent(): IScope;
      function GetMemberCurrent(const AMemberName: String): IScope;
      function MoveNext(): Boolean;
    public
      constructor Create(const AObject: TObject; const AParentScope: IScope = nil);
      destructor Destroy(); override;
    end;
  const
    AUTO_OPTIONS_DEFAULT = [TAutoOption.Create, TAutoOption.Destroy, TAutoOption.Activate];
  var
    FActive              : Boolean;
    FAutoOptions         : TAutoOptions;
    FViewModelClass      : String;
    FViewModel           : TCustomViewModel;
    FOnViewModelInit     : TNotifyEvent;
    function GetHasViewModel(): Boolean; inline;
    procedure SetViewModelClass(const AValue: String);
    function GetMemberByName(const ASource: TObject; const AMemberName: String): TObject;
    procedure SetActive(const AValue: Boolean);
    procedure SetViewModel(const AValue: TCustomViewModel);
    { IScopeActive }
    function GetActive(): Boolean;
    { IScopeRecordEnumerable }
    function GetEnumerator(const AMemberName: String; ABufferCount: Integer): IScopeRecordEnumerator;
  protected
    function GetValue(): TObject; override;
    function GetMember(const AMemberName: String): TObject; override;
    procedure Loaded(); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy(); override;

    function ViewModelAs<T: TCustomViewModel>(): T; inline;

    property HasViewModel: Boolean read GetHasViewModel;
    property Active: Boolean read GetActive write SetActive;
    property ViewModel: TCustomViewModel read FViewModel write SetViewModel;
  published
    property ScopeMappings;

    property AutoOptions: TAutoOptions read FAutoOptions write FAutoOptions default AUTO_OPTIONS_DEFAULT;
    property ViewModelClass: String read FViewModelClass write SetViewModelClass;

    property OnViewModelInit: TNotifyEvent read FOnViewModelInit write FOnViewModelInit;
  end;

implementation

{$REGION 'TScopeRecordEnumerator'}
{ TScopeRecordEnumerator }

constructor TViewModelAdapter.TScopeRecordEnumerator.Create(const AObject: TObject; const AParentScope: IScope = nil);
begin
  inherited Create();
  FObject := AObject;
  FParentScope := AParentScope;
end;

destructor TViewModelAdapter.TScopeRecordEnumerator.Destroy();
begin
  FreeAndNil(FEnumerator);
  inherited;
end;

procedure TViewModelAdapter.TScopeRecordEnumerator.First();
var
  LContext  : TRttiContext;
  LRttiType : TRttiType;
  LMethod   : TRttiMethod;
  LArgs     : TArray<TValue>;
  LValue    : TValue;
begin
  FMoveNext := nil;
  FreeAndNil(FEnumerator);
  LRttiType := LContext.GetType(FObject.ClassType);
  if (LRttiType = nil) then begin
    Exit;
  end;
  LMethod := LRttiType.GetMethod('GetEnumerator');
  if (LMethod = nil) then begin
    Exit;
  end;
  LValue := LMethod.Invoke(FObject, LArgs);
  if (LValue.IsObject) then begin
    FEnumerator := LValue.AsObject();
  end;
  if (FEnumerator = nil) then begin
    Exit;
  end;
  LRttiType := LContext.GetType(FEnumerator.ClassType);
  FMoveNext := LRttiType.GetMethod('MoveNext');
end;

function TViewModelAdapter.TScopeRecordEnumerator.GetCurrent(): IScope;
begin
  if (FEnumerator = nil) then begin
    Exit(nil);
  end;
  Result := WrapObject(FEnumerator);
  if (FParentScope <> nil) then begin
    Result := TNestedScope.Create(Result, FParentScope);
  end;
end;

function TViewModelAdapter.TScopeRecordEnumerator.GetMemberCurrent(const AMemberName: String): IScope;
begin
  Result := GetCurrent();
end;

function TViewModelAdapter.TScopeRecordEnumerator.MoveNext(): Boolean;
var
  LArgs   : TArray<TValue>;
  LValue  : TValue;
begin
  if (FEnumerator = nil) or (FMoveNext = nil) then begin
    Exit(False);
  end;
  LValue := FMoveNext.Invoke(FEnumerator, LArgs);
  Result := (LValue.IsType<Boolean>) and (LValue.AsBoolean);
end;
{$ENDREGION 'TScopeRecordEnumerator'}

{$REGION 'TViewModelAdapter'}
{ TViewModelAdapter }

constructor TViewModelAdapter.Create(AOwner: TComponent);
begin
  inherited;
  FAutoOptions := AUTO_OPTIONS_DEFAULT;
end;

destructor TViewModelAdapter.Destroy();
begin
  ViewModel := nil;
  inherited;
end;

function TViewModelAdapter.GetActive(): Boolean;
begin
  Result := FActive;
end;

function TViewModelAdapter.GetEnumerator(const AMemberName: String; ABufferCount: Integer): IScopeRecordEnumerator;
var
  LMember : TObject;
begin
  LMember := GetMember(AMemberName);
  if (LMember = nil) then begin
    Exit(nil);
  end;
  Result := TScopeRecordEnumerator.Create(LMember, AddScopeMappings(nil));
end;

function TViewModelAdapter.GetHasViewModel(): Boolean;
begin
  Result := FViewModel <> nil;
end;

function TViewModelAdapter.GetMember(const AMemberName: String): TObject;
var
  LMemberName : String;
begin
  if (AMemberName.IsEmpty) then begin
    Exit(nil);
  end;
  if (FViewModel = nil) then begin
    Exit(nil);
  end;
  Result := FViewModel;
  // Get source object by path.
  for LMemberName in AMemberName.Split(['.']) do begin
    Result := GetMemberByName(Result, LMemberName);
    if (Result = nil) then begin
      Break;
    end;
  end;
end;

function TViewModelAdapter.GetMemberByName(const ASource: TObject; const AMemberName: String): TObject;
var
  LContext  : TRttiContext;
  LRttiType : TRttiType;
  LProperty : TRttiProperty;
  LValue    : TValue;
begin
  if (AMemberName.IsEmpty) then begin
    Exit(nil);
  end;
  // Getting member object by name from source object.
  LRttiType := LContext.GetType(ASource.ClassType);
  if (LRttiType = nil) then begin
    Exit(nil);
  end;
  LProperty := LRttiType.GetProperty(AMemberName);
  if (LProperty = nil) then begin
    Exit(nil);
  end;
  LValue := LProperty.GetValue(ASource);
  {$IF CompilerVersion < 31}
  if (not LValue.IsObject) or (LValue.AsObject = nil) then begin
  {$ELSE}
  if (not LValue.IsObjectInstance) then begin
  {$ENDIF}
    Exit(nil);
  end;
  Result := LValue.AsObject;
end;

function TViewModelAdapter.GetValue(): TObject;
begin
  Result := FViewModel;
end;

procedure TViewModelAdapter.Loaded();
begin
  inherited;
  try
    if (csDesigning in ComponentState) then begin
      Exit;
    end;
    if (TAutoOption.Create in FAutoOptions) then begin
      ViewModel := TViewModelClass.CreateViewModel(FViewModelClass);
    end;
  except
    on E: Exception do begin
      // Skip all exceptions while loading.
    end;
  end;
end;

procedure TViewModelAdapter.SetActive(const AValue: Boolean);
var
  LActive : Boolean;
begin
  LActive := (AValue) and (FViewModel <> nil);
  if (FActive <> LActive) then begin
    FActive := LActive;
    ActivateExpressions(FActive);
  end;
end;

procedure TViewModelAdapter.SetViewModel(const AValue: TCustomViewModel);
begin
  if (FViewModel = AValue) then begin
    Exit;
  end;
  Active := False;
  if (TAutoOption.Destroy in FAutoOptions) then begin
    FreeAndNil(FViewModel);
  end;
  FViewModel := AValue;
  if (FViewModel = nil) then begin
    Exit;
  end;
  if (Assigned(FOnViewModelInit)) then begin
    FOnViewModelInit(Self);
  end;
  if (TAutoOption.Activate in FAutoOptions) then begin
    Active := True;
  end;
end;

procedure TViewModelAdapter.SetViewModelClass(const AValue: String);
begin
  if (FViewModelClass <> AValue) then begin
    FViewModelClass := AValue;
  end;
end;

function TViewModelAdapter.ViewModelAs<T>(): T;
begin
  Result := ViewModel as T;
end;
{$ENDREGION 'TViewModelAdapter'}

end.

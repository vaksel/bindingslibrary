//  Copyright 2018 Viktor Akselrod
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

unit Bindings.Notifications.Observers;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Rtti,
  System.Bindings.Helper,
  System.DelayProc;

type
  TBindingObserver = class abstract(TInterfacedObject, IObserver)
  strict private
  var
    FComponent : TComponent;
    FProperty  : String;
    FActive    : Boolean;
    FOnToggle  : TObserverToggleEvent;
    { IObserver }
    procedure Removed();
    function GetActive(): Boolean;
    procedure SetActive(AValue: Boolean);
    function GetOnObserverToggle(): TObserverToggleEvent;
    procedure SetOnObserverToggle(AEvent: TObserverToggleEvent);
  protected
    procedure DoNotify();
  public
    constructor Create(const AComponent: TComponent; const AProperty: String = '');
  end;

  TBindingSingleCastObserver = class abstract(TBindingObserver, ISingleCastObserver)
  end;

  TBindingMultiCastObserver = class abstract(TBindingObserver, IMultiCastObserver)
  end;

  TBindingTrackObserver = class abstract(TBindingMultiCastObserver, IObserverTrack)
  strict private
  var
    FDelayProc  : TDelayProc;
    FTrackDelay : UInt32;
    function GetTrack(): Boolean;
  protected
    procedure DoTrack();
    property Track: Boolean read GetTrack;
  public
    constructor Create(const AComponent: TComponent; const AProperty: String = '';
      const ATrack: Boolean = False; const ATrackDelay: UInt32 = 0);
    destructor Destroy(); override;
  end;

  TBindingPositionLinkObserver = class(TBindingTrackObserver, IPositionLinkObserver)
  strict private
    { IPositionLinkObserver }
    procedure PosChanged();
    procedure PosChanging();
  public
    constructor Create(const AComponent: TComponent; const AProperty: String = '';
      const ATrack: Boolean = False; const ATrackDelay: UInt32 = 0);
  end;

  TBindingControlValueObserver = class(TBindingTrackObserver, IControlValueObserver)
  strict private
  var
    FModified : Boolean;
    { IControlValueObserver }
    procedure ValueModified();
    procedure ValueUpdate();
  public
    constructor Create(const AComponent: TComponent; const AProperty: String = '';
      const ATrack: Boolean = False; const ATrackDelay: UInt32 = 0);
  end;

  TBindingsClickObserver = class(TComponent)
  strict private
  var
    FClick              : Boolean;
    FDoubleClick        : Boolean;
    FAttachedCount      : Int32;
    FStoreOnClick       : TNotifyEvent;
    FStoreOnDoubleClick : TNotifyEvent;
    function GetIsAttached(): Boolean;
  public
    procedure OnClick(Sender: TObject);
    procedure OnDoubleClick(Sender: TObject);

    procedure Attach();
    procedure Detach();

    property IsAttached: Boolean read GetIsAttached;
    property StoreOnClick: TNotifyEvent read FStoreOnClick write FStoreOnClick;
    property StoreOnDoubleClick: TNotifyEvent read FStoreOnDoubleClick write FStoreOnDoubleClick;
    property Click: Boolean read FClick;
    property DoubleClick: Boolean read FDoubleClick;
  end;

implementation

{ TBindingObserver }

constructor TBindingObserver.Create(const AComponent: TComponent;
  const AProperty: String);
begin
  inherited Create();
  ASSERT(AComponent <> nil);
  FActive := True;
  FComponent := AComponent;
  FProperty := AProperty;
end;

procedure TBindingObserver.DoNotify();
begin
  if (FActive) then begin
    TBindings.Notify(FComponent, FProperty);
  end;
end;

function TBindingObserver.GetActive(): Boolean;
begin
  Result := FActive;
end;

function TBindingObserver.GetOnObserverToggle(): TObserverToggleEvent;
begin
  Result := FOnToggle;
end;

procedure TBindingObserver.Removed();
begin
  SetActive(False);
end;

procedure TBindingObserver.SetActive(AValue: Boolean);
begin
  FActive := AValue;
  if (Assigned(FOnToggle)) then begin
    FOnToggle(Self, AValue);
  end;
end;

procedure TBindingObserver.SetOnObserverToggle(AEvent: TObserverToggleEvent);
begin
  FOnToggle := AEvent;
end;

{ TBindingTrackObserver }

constructor TBindingTrackObserver.Create(const AComponent: TComponent; const AProperty: String;
  const ATrack: Boolean; const ATrackDelay: UInt32);
begin
  inherited Create(AComponent, AProperty);
  FTrackDelay := ATrackDelay;
  if (ATrack) then begin
    FDelayProc := TDelayProc.Create();
  end;
end;

destructor TBindingTrackObserver.Destroy();
begin
  FreeAndNil(FDelayProc);
  inherited;
end;

function TBindingTrackObserver.GetTrack(): Boolean;
begin
  Result := FDelayProc <> nil;
end;

procedure TBindingTrackObserver.DoTrack();
begin
  if (not Track) then begin
    Exit;
  end;
  FDelayProc.Execute(FTrackDelay,
    procedure ()
    begin
      DoNotify();
    end);
end;

{ TBindingPositionLinkObserver }

constructor TBindingPositionLinkObserver.Create(const AComponent: TComponent; const AProperty: String;
  const ATrack: Boolean; const ATrackDelay: UInt32);
begin
  inherited;
  AComponent.Observers.AddObserver(TObserverMapping.PositionLinkID, Self);
end;

procedure TBindingPositionLinkObserver.PosChanged();
begin
  DoNotify();
end;

procedure TBindingPositionLinkObserver.PosChanging();
begin
  DoTrack();
end;

{ TBindingControlValueObserver }

constructor TBindingControlValueObserver.Create(const AComponent: TComponent;
  const AProperty: String; const ATrack: Boolean; const ATrackDelay: UInt32);
begin
  inherited;
  AComponent.Observers.AddObserver(TObserverMapping.ControlValueID, Self);
end;

procedure TBindingControlValueObserver.ValueModified();
begin
  FModified := True;
  DoTrack();
end;

procedure TBindingControlValueObserver.ValueUpdate();
begin
  if (FModified) then begin
    FModified := False;
    DoNotify();
  end;
end;

{ TBindingsClickObserver }

procedure TBindingsClickObserver.Attach();
begin
  Inc(FAttachedCount);
end;

procedure TBindingsClickObserver.Detach();
begin
  Dec(FAttachedCount);
  if (FAttachedCount < 0) then begin
    Error(reInvalidPtr);
  end;
end;

procedure TBindingsClickObserver.OnClick(Sender: TObject);
begin
  FClick := True;
  try
    TBindings.Notify(Self, 'Click');
  finally
    FClick := False;
  end;
  if (Assigned(FStoreOnClick)) then begin
    FStoreOnClick(Owner);
  end;
end;

procedure TBindingsClickObserver.OnDoubleClick(Sender: TObject);
begin
  FDoubleClick := True;
  try
    TBindings.Notify(Self, 'DoubleClick');
  finally
    FDoubleClick := False;
  end;
  if (Assigned(FStoreOnDoubleClick)) then begin
    FStoreOnDoubleClick(Owner);
  end;
end;

function TBindingsClickObserver.GetIsAttached(): Boolean;
begin
  Result := FAttachedCount > 0;
end;

end.

//  Copyright 2018 Viktor Akselrod
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

unit Bindings.Registration;

interface

uses
  System.Classes,
  Data.Bind.Components,
  Bindings.Expressions.Components,
  Bindings.Expressions.Scope,
  Bindings.Expressions.Designer,
  Bindings.ViewModelAdapter;

procedure Register();

implementation

procedure Register();
begin
  RegisterBindComponents('BindingsLibrary', [TBindingList, TBindingControl, TBindingCommand]);
  RegisterComponents('BindingsLibrary', [TBindingsScope, TViewModelAdapter]);
  RegisterBindCompDesigner(TBindingControl, TBindingControlDesigner.Create());
  RegisterBindCompDesigner(TBindingCommand, TBindingCommandDesigner.Create());
end;

end.

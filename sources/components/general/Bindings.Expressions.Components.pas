//  Copyright 2018 Viktor Akselrod
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

unit Bindings.Expressions.Components;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Math,
  System.Generics.Collections,
  Data.Bind.Components,
  System.Bindings.Consts,
  System.Bindings.EvalProtocol,
  System.Bindings.Methods,
  System.Bindings.Outputs,
  System.Bindings.Expression,
  System.Bindings.Manager,
  System.Bindings.ObjEval,
  System.Bindings.Helper,
  Bindings.ViewModelAdapter,
  Bindings.Notifications.Observers,
  Bindings.Notifications.Handler;

type
  TExpressionList = TObjectList<TBindingExpression>;

  TControlProperties = class(TPersistent)
  protected
  type
    TControlOption = class(TObject)
      ControlProperty : String;
      SourceProperty  : String;
      Enabled         : Boolean;
    end;
    TCommonBindComponentOpen = class(TCommonBindComponent);
  var
    FOwner        : TCommonBindComponentOpen;
    FOptions      : TObjectList<TControlOption>;
    FExpressions  : TExpressionList;
  protected
    function GetOptionEnabled(const AIndex: Int32): Boolean;
    procedure SetOptionEnabled(const AIndex: Int32; const AValue: Boolean);
    procedure AddOption(const AControlProperty, ASourceProperty: String; const AEnabled: Boolean);
    procedure AssignTo(ADest: TPersistent); override;
    procedure ActivateOption(const AOption: TControlOption); virtual;
    procedure InitOptions(); virtual;
  public
    constructor Create(const AOwner: TCommonBindComponent);
    destructor Destroy(); override;

    procedure Activate();
    procedure Deactivate();
  published
    property Enabled: Boolean index 0 read GetOptionEnabled write SetOptionEnabled default False;
    property Visible: Boolean index 1 read GetOptionEnabled write SetOptionEnabled default False;
    property Hint: Boolean index 2 read GetOptionEnabled write SetOptionEnabled default False;
    property Text: Boolean index 3 read GetOptionEnabled write SetOptionEnabled default False;
  end;

  TCustomNotification = class abstract(TPersistent)
  strict private
  var
    FComponent    : TComponent;
    FEnabled      : Boolean;
    FOnChange     : TProc<TObject>;
    procedure SetEnabled(const AValue: Boolean);
  protected
    procedure AssignTo(ADest: TPersistent); override;
    procedure SetComponent(const AValue: TComponent); virtual;
    function GetNotifySource(): TComponent; virtual;
    procedure Change(); inline;
  public
    constructor Create();

    procedure Activate(); virtual; abstract;
    procedure Deactivate(); virtual; abstract;

    property Component: TComponent read FComponent write SetComponent;
    property NotifySource: TComponent read GetNotifySource;

    property OnChange: TProc<TObject> read FOnChange write FOnChange;
  published
    property Enabled: Boolean read FEnabled write SetEnabled default True;
  end;

  TChangeNotification = class(TCustomNotification)
  strict private
  var
    FPropertyName : String;
    FTrack        : Boolean;
    FTrackDelay   : UInt32;
    FNotification : TChangeNotificationHandle;
    procedure SetPropertyName(const AValue: String);
    procedure SetTrackChanges(const AValue: Boolean);
    procedure SetTrackDelay(const AValue: UInt32);
  protected
    procedure AssignTo(ADest: TPersistent); override;
    procedure SetComponent(const AValue: TComponent); override;
  public
    constructor Create();
    destructor Destroy; override;

    procedure Activate(); override;
    procedure Deactivate(); override;
  published
    property PropertyName: String read FPropertyName write SetPropertyName;
    property Track: Boolean read FTrack write SetTrackChanges default True;
    property TrackDelay: UInt32 read FTrackDelay write SetTrackDelay default 0;
  end;

  TClickKind = (
    OneClick,
    DoubleClick
  );

  TClickNotification = class(TCustomNotification)
  strict private
  var
    FNotification : TClickNotificationHandle;
    FClickKind    : TClickKind;
    procedure SetClickKind(const AValue: TClickKind);
  protected
    procedure AssignTo(ADest: TPersistent); override;
    function GetNotifySource(): TComponent; override;
  public
    destructor Destroy; override;

    procedure Activate(); override;
    procedure Deactivate(); override;
  published
    property ClickKind: TClickKind read FClickKind write SetClickKind default TClickKind.OneClick;
  end;

  TBindingCustomControl = class(TBaseBindExpression)
  strict private
  var
    FExpressions          : TExpressionList;
    FControlProperties    : TControlProperties;
    FControlNotification  : TCustomNotification;
    procedure SetControlProperties(const AValue: TControlProperties);
    procedure SetCustomNotification(const AValue: TCustomNotification);
  protected
    procedure FreeExpressionsObjects(); override;
    procedure DoOnActivating(); override;
    procedure DoOnDeactivated(); override;
    procedure UpdateControlChanged(); override;
    function CreateControlNotifications(): TCustomNotification; virtual;
    property Expressions: TExpressionList read FExpressions;
    property CustomNotification: TCustomNotification read FControlNotification write SetCustomNotification;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy(); override;
  published
    property ControlProperties: TControlProperties read FControlProperties write SetControlProperties;
    property ControlComponent;
    property SourceComponent;
    property SourceMemberName;
    property AutoActivate;
    property NotifyOutputs default False;
    property OnAssigningValue;
    property OnAssignedValue;
    property OnEvalError;
    property OnActivating;
    property OnActivated;
  end;

  TBindingMode = (
    OneWay,
    TwoWay
  );

  TBindingControl = class(TBindingCustomControl)
  strict private
  var
    FSourceExpression        : String;
    FControlExpression       : String;
    FBindingMode             : TBindingMode;
    FInitializeControlValue  : Boolean;
    function GetControlNotification(): TChangeNotification;
    procedure SetControlNotification(const AValue: TChangeNotification);
    procedure SetControlExpression(const AValue: String);
    procedure SetSourceExpression(const AValue: String);
    procedure SetDirection(const AValue: TBindingMode);
    procedure SetInitializeControlValue(const AValue: Boolean);
  protected
    function CreateControlNotifications(): TCustomNotification; override;
    procedure UpdateExpressions(); override;
    function CanActivate(): Boolean; override;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property ControlNotification: TChangeNotification read GetControlNotification write SetControlNotification;
    property SourceExpression: String read FSourceExpression write SetSourceExpression;
    property ControlExpression: String read FControlExpression write SetControlExpression;
    property BindingMode: TBindingMode read FBindingMode write SetDirection default TBindingMode.TwoWay;
    property InitializeControlValue: Boolean read FInitializeControlValue write SetInitializeControlValue default True;
  end;

  TBindingCommand = class(TBindingCustomControl)
  strict private
  var
    FSourceCommand  : String;
    function GetControlNotification(): TClickNotification;
    procedure SetControlNotification(const AValue: TClickNotification);
    procedure SetSourceCommand(const AValue: String);
  protected
    function CreateControlNotifications(): TCustomNotification; override;
    procedure UpdateExpressions(); override;
    function CanActivate(): Boolean; override;
  published
    property ControlNotification: TClickNotification read GetControlNotification write SetControlNotification;
    property SourceCommand: String read FSourceCommand write SetSourceCommand;
  end;

  TBindingList = class(TCustomBindList)
  strict private
  var
    FAutoUpdate           : Boolean;
    FAutoUpdateExpression : TBindingExpression;
    procedure SetAutoUpdate(const AValue: Boolean);
  protected
    procedure DoOnActivating(); override;
    procedure DoOnDeactivated(); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy(); override;

    procedure UpdateList();
  published
    property AutoUpdate: Boolean read FAutoUpdate write SetAutoUpdate default True;
    property ControlComponent;
    property SourceComponent;
    property SourceMemberName;
    property FormatExpressions;
    property FormatControlExpressions;
    property ClearControlExpressions;
    property AutoFill;
    property AutoActivate;
    property OnAssigningValue;
    property OnAssignedValue;
    property OnEvalError;
    property OnActivating;
    property OnActivated;
    property Editor;
  end;

implementation

{$REGION 'TDesignerHelper'}
type
  TDesignerHelper = record
  public
    class function IsDesigning(const ASource: TComponent): Boolean; static;
    class function FindBindingsScope(const ASource: TComponent): TViewModelAdapter; static;
  end;

{ TDesignerHelper }

class function TDesignerHelper.IsDesigning(const ASource: TComponent): Boolean;
begin
  Result := (ASource.ComponentState * [csDesigning, csLoading] = [csDesigning]) and
    ((ASource.Owner = nil) or (IsDesigning(ASource.Owner)));
end;

class function TDesignerHelper.FindBindingsScope(const ASource: TComponent): TViewModelAdapter;
var
  LOwner  : TComponent;
  I       : Int32;
begin
  if (ASource = nil) then begin
    Exit(nil);
  end;
  LOwner := ASource.Owner;
  while (LOwner <> nil) do begin
    for I := 0 to LOwner.ComponentCount - 1 do begin
      if (LOwner.Components[I] is TViewModelAdapter) then begin
        Exit(TViewModelAdapter(LOwner.Components[I]));
      end;
    end;
    LOwner := LOwner.Owner;
  end;
  Result := nil;
end;
{$ENDREGION 'TDesignerHelper'}

{$REGION 'TControlProperties'}
{ TControlProperties }

constructor TControlProperties.Create(const AOwner: TCommonBindComponent);
begin
  inherited Create();
  FOwner := TCommonBindComponentOpen(AOwner);
  FOptions := TObjectList<TControlOption>.Create();
  FExpressions := TExpressionList.Create();
  InitOptions();
end;

destructor TControlProperties.Destroy();
begin
  FreeAndNil(FExpressions);
  FreeAndNil(FOptions);
  inherited;
end;

procedure TControlProperties.AssignTo(ADest: TPersistent);
begin
  if (ADest is TControlProperties) then begin
    // Need review! Copying only links for objects.
    TControlProperties(ADest).FOptions.DeleteRange(0, FOptions.Count);
    TControlProperties(ADest).FOptions.InsertRange(0, FOptions);
  end else begin
    inherited;
  end;
end;

procedure TControlProperties.InitOptions();
begin
  AddOption('Enabled', 'Enabled.Value', False);
  AddOption('Visible', 'Visible.Value', False);
  AddOption('Hint', 'Hint.Value', False);
  AddOption('Text', 'Text.Value', False);
end;

procedure TControlProperties.ActivateOption(const AOption: TControlOption);
var
  LControlScopes    : TArray<IScope>;
  LSourceScopes     : TArray<IScope>;
  LOutputConverters : IValueRefConverter;
  LExpression       : TBindingExpression;
begin
  if (not AOption.Enabled) then begin
    Exit;
  end;
  LControlScopes := FOwner.GetControlScopes();
  LSourceScopes := FOwner.GetSourceScopes();
  LOutputConverters := FOwner.GetOutputConverter();
  // Source to control.
  LExpression := TBindings.CreateManagedBinding(
    LSourceScopes,
    AOption.SourceProperty,
    LControlScopes,
    AOption.ControlProperty,
    LOutputConverters,
    nil,
    [TBindings.TCreateOption.coEvaluate]);
  FExpressions.Add(LExpression);
end;

procedure TControlProperties.AddOption(const AControlProperty, ASourceProperty: String; const AEnabled: Boolean);
var
  LOption : TControlOption;
begin
  LOption := TControlOption.Create();
  LOption.ControlProperty := AControlProperty;
  LOption.SourceProperty := ASourceProperty;
  LOption.Enabled := AEnabled;
  FOptions.Add(LOption);
end;

procedure TControlProperties.Activate();
var
  LOption : TControlOption;
begin
  for LOption in FOptions do begin
    ActivateOption(LOption);
  end;
end;

procedure TControlProperties.Deactivate();
begin
  FExpressions.Clear();
end;

function TControlProperties.GetOptionEnabled(const AIndex: Int32): Boolean;
begin
  Result := (InRange(AIndex, 0, FOptions.Count - 1)) and (FOptions[AIndex].Enabled);
end;

procedure TControlProperties.SetOptionEnabled(const AIndex: Int32; const AValue: Boolean);
begin
  if (InRange(AIndex, 0, FOptions.Count - 1)) then begin
    FOptions[AIndex].Enabled := AValue;
  end;
end;
{$ENDREGION 'TControlProperties'}

{$REGION 'TCustomNotification'}
{ TCustomNotification }

constructor TCustomNotification.Create();
begin
  inherited;
  FEnabled := True;
end;

procedure TCustomNotification.AssignTo(ADest: TPersistent);
begin
  if (ADest is TCustomNotification) then begin
    TCustomNotification(ADest).FEnabled := FEnabled;
  end else begin
    inherited;
  end;
end;

procedure TCustomNotification.Change();
begin
  if (Assigned(FOnChange)) then begin
    FOnChange(Self);
  end;
end;

function TCustomNotification.GetNotifySource(): TComponent;
begin
  Result := FComponent;
end;

procedure TCustomNotification.SetComponent(const AValue: TComponent);
begin
  if (FComponent <> AValue) then begin
    FComponent := AValue;
    Change();
  end;
end;

procedure TCustomNotification.SetEnabled(const AValue: Boolean);
begin
  if (FEnabled <> AValue) then begin
    FEnabled := AValue;
    Change();
  end;
end;
{$ENDREGION 'TCustomNotification'}

{$REGION 'TChangeNotification'}
{ TChangeNotification }

constructor TChangeNotification.Create();
begin
  inherited;
  FTrack := True;
end;

destructor TChangeNotification.Destroy;
begin
  Deactivate;
  inherited;
end;

procedure TChangeNotification.AssignTo(ADest: TPersistent);
begin
  inherited;
  if (ADest is TChangeNotification) then begin
    TChangeNotification(ADest).FPropertyName := FPropertyName;
    TChangeNotification(ADest).FTrack := FTrack;
    TChangeNotification(ADest).FTrackDelay := FTrackDelay;
  end;
end;

procedure TChangeNotification.Activate();
begin
  if (Enabled) and (Component <> nil) then begin
    FNotification := TControlNotifications.Current.ActivateChange(Component, FPropertyName, FTrack, FTrackDelay);
  end;
end;

procedure TChangeNotification.Deactivate();
begin
  if FNotification <> nil then
  begin
    TControlNotifications.Current.DeactivateChange(FNotification);
    FreeAndNil(FNotification);
  end;
end;

procedure TChangeNotification.SetComponent(const AValue: TComponent);
begin
  inherited;
  if (Component <> nil) and (TDesignerHelper.IsDesigning(Component)) then begin
    if (FPropertyName.IsEmpty) then begin
      PropertyName := TControlNotifications.Current.GetNotifyProperty(Component);
    end;
  end;
end;

procedure TChangeNotification.SetPropertyName(const AValue: String);
begin
  if (FPropertyName <> AValue) then begin
    FPropertyName := AValue;
    Change();
  end;
end;

procedure TChangeNotification.SetTrackChanges(const AValue: Boolean);
begin
  if (FTrack <> AValue) then begin
    FTrack := AValue;
    Change();
  end;
end;

procedure TChangeNotification.SetTrackDelay(const AValue: UInt32);
begin
  if (FTrackDelay <> AValue) then begin
    FTrackDelay := AValue;
    Change();
  end;
end;
{$ENDREGION 'TChangeNotification'}

{$REGION 'TClickNotification'}
{ TClickNotification }

destructor TClickNotification.Destroy;
begin
  Deactivate;
  inherited;
end;

procedure TClickNotification.AssignTo(ADest: TPersistent);
begin
  inherited;
  if (ADest is TClickNotification) then begin
    TClickNotification(ADest).FClickKind := FClickKind;
  end;
end;

procedure TClickNotification.Activate();
begin
  if (Component <> nil) then begin
    FNotification := TControlNotifications.Current.ActivateClick(Component);
  end;
end;

procedure TClickNotification.Deactivate();
begin
  if FNotification <> nil then
  begin
    TControlNotifications.Current.DeactivateClick(FNotification);
    FreeAndNil(FNotification);
  end;
end;

function TClickNotification.GetNotifySource(): TComponent;
begin
  if (FNotification <> nil) then begin
    Result := FNotification.Observer;
  end else begin
    Result := nil;
  end;
end;

procedure TClickNotification.SetClickKind(const AValue: TClickKind);
begin
  if (FClickKind <> AValue) then begin
    FClickKind := AValue;
    Change();
  end;
end;
{$ENDREGION 'TClickNotification'}

{$REGION 'TBindingCustomControl'}
{ TBindingCustomControl }

constructor TBindingCustomControl.Create(AOwner: TComponent);
begin
  inherited;
  FExpressions := TExpressionList.Create();
  FControlProperties := TControlProperties.Create(Self);
  if (TDesignerHelper.IsDesigning(Self)) then begin
    SourceComponent := TDesignerHelper.FindBindingsScope(Self);
  end;
  FControlNotification := CreateControlNotifications();
end;

destructor TBindingCustomControl.Destroy();
begin
  FreeAndNil(FExpressions);
  FreeAndNil(FControlNotification);
  FreeAndNil(FControlProperties);
  inherited;
end;

function TBindingCustomControl.CreateControlNotifications(): TCustomNotification;
begin
  Result := nil;
end;

procedure TBindingCustomControl.FreeExpressionsObjects();
begin
  inherited;
  FExpressions.Clear();
end;

procedure TBindingCustomControl.DoOnActivating();
begin
  inherited;
  FControlProperties.Activate();
  if (FControlNotification <> nil) then begin
    FControlNotification.Activate();
  end;
end;

procedure TBindingCustomControl.DoOnDeactivated();
begin
  inherited;
  FControlProperties.Deactivate();
  if (FControlNotification <> nil) then begin
    FControlNotification.Deactivate();
  end;
end;

procedure TBindingCustomControl.UpdateControlChanged();
begin
  inherited;
  if (FControlNotification <> nil) then begin
    FControlNotification.Component := ControlComponent;
  end;
end;

procedure TBindingCustomControl.SetCustomNotification(const AValue: TCustomNotification);
begin
  if (AValue <> nil) and (AValue <> FControlNotification) then begin
    FControlNotification.Assign(AValue);
  end;
end;

procedure TBindingCustomControl.SetControlProperties(const AValue: TControlProperties);
begin
  if (AValue <> nil) and (AValue <> FControlProperties) then begin
    FControlProperties.Assign(AValue);
  end;
end;
{$ENDREGION 'TBindingCustomControl'}

{$REGION 'TBindingControl'}
{ TBindingControl }

constructor TBindingControl.Create(AOwner: TComponent);
begin
  inherited;
  ControlNotification.OnChange :=
    procedure (Sender: TObject)
    begin
      Active := False;
      if (ControlExpression.IsEmpty) then begin
        ControlExpression := ControlNotification.PropertyName;
      end;
    end;
  FBindingMode := TBindingMode.TwoWay;
  FInitializeControlValue := True;
end;

function TBindingControl.CreateControlNotifications(): TCustomNotification;
begin
  Result := TChangeNotification.Create();
end;

procedure TBindingControl.SetDirection(const AValue: TBindingMode);
begin
  if (FBindingMode <> AValue) then begin
    Active := False;
    FBindingMode := AValue;
  end;
end;

procedure TBindingControl.SetInitializeControlValue(const AValue: Boolean);
begin
  if (FInitializeControlValue <> AValue) then begin
    Active := False;
    FInitializeControlValue := AValue;
  end;
end;

procedure TBindingControl.SetControlExpression(const AValue: String);
begin
  if (FControlExpression <> AValue) then begin
    Active := False;
    FControlExpression := AValue;
  end;
end;

procedure TBindingControl.SetControlNotification(const AValue: TChangeNotification);
begin
  CustomNotification := AValue;
end;

procedure TBindingControl.SetSourceExpression(const AValue: String);
begin
  if (FSourceExpression <> AValue) then begin
    Active := False;
    FSourceExpression := AValue;
  end;
end;

function TBindingControl.CanActivate(): Boolean;
begin
  Result := (inherited) and (not String.IsNullOrWhiteSpace(FControlExpression))
    and (not String.IsNullOrWhiteSpace(FSourceExpression))
end;

function TBindingControl.GetControlNotification(): TChangeNotification;
begin
  Result := CustomNotification as TChangeNotification;
end;

procedure TBindingControl.UpdateExpressions();
var
  LControlScopes    : TArray<IScope>;
  LSourceScopes     : TArray<IScope>;
  LOutputConverters : IValueRefConverter;
  LBindingEvents    : TBindingEventRec;
  LOptions          : TBindings.TCreateOptions;
begin
  if (Designing) then begin
    Exit;
  end;
  LControlScopes := GetControlScopes();
  LSourceScopes := GetSourceScopes();
  LOutputConverters := GetOutputConverter();
  LBindingEvents := TBindingEventRec.Create(DoOnEvalError, DoOnAssigningValue,
    DoOnAssignedValue, DoOnLocationUpdated);
  LOptions := [];
  if (FInitializeControlValue) then begin
    Include(LOptions, TBindings.TCreateOption.coEvaluate);
  end;
  if (NotifyOutputs) then begin
    Include(LOptions, TBindings.TCreateOption.coNotifyOutput);
  end;
  Expressions.Add(TBindings.CreateManagedBinding(LSourceScopes, FSourceExpression,
    LControlScopes, FControlExpression, LOutputConverters, LBindingEvents, nil, LOptions));
  if (FBindingMode = TBindingMode.TwoWay) then begin
    Expressions.Add(TBindings.CreateManagedBinding(LControlScopes, FControlExpression,
      LSourceScopes, FSourceExpression, LOutputConverters, LBindingEvents, nil, LOptions - [TBindings.TCreateOption.coEvaluate]));
  end;
end;
{$ENDREGION 'TBindingControl'}

{$REGION 'TBindingCommand'}
{ TBindingCommand }

function TBindingCommand.CreateControlNotifications(): TCustomNotification;
begin
  Result := TClickNotification.Create();
end;

function TBindingCommand.CanActivate(): Boolean;
begin
  Result := (inherited) and (not String.IsNullOrWhiteSpace(FSourceCommand));
end;

function TBindingCommand.GetControlNotification(): TClickNotification;
begin
  Result := CustomNotification as TClickNotification;
end;

procedure TBindingCommand.SetControlNotification(const AValue: TClickNotification);
begin
  CustomNotification := AValue;
end;

procedure TBindingCommand.SetSourceCommand(const AValue: String);
begin
  if (FSourceCommand <> AValue) then begin
    Active := False;
    FSourceCommand := AValue;
  end;
end;

procedure TBindingCommand.UpdateExpressions();
var
  LControlExpr : String;
  LOptions     : TBindings.TCreateOptions;
begin
  if (Designing) then begin
    Exit;
  end;
  if (ControlNotification.ClickKind = TClickKind.DoubleClick) then begin
    LControlExpr := 'DoubleClick';
  end else begin
    LControlExpr := 'Click';
  end;
  if (NotifyOutputs) then begin
    LOptions := [TBindings.TCreateOption.coNotifyOutput];
  end else begin
    LOptions := [];
  end;
  Expressions.Add(TBindings.CreateManagedBinding(GetComponentScopes(CustomNotification.NotifySource),
    LControlExpr, GetSourceScopes(), FSourceCommand + '.Exec', GetOutputConverter(),
    TBindingEventRec.Create(DoOnEvalError, DoOnAssigningValue, DoOnAssignedValue, DoOnLocationUpdated),
    nil, LOptions));
end;
{$ENDREGION 'TBindingCommand'}

{$REGION 'TBindingList'}
{ TBindingList }

constructor TBindingList.Create(AOwner: TComponent);
begin
  inherited;
  FAutoUpdate := True;
  if (TDesignerHelper.IsDesigning(Self)) then begin
    SourceComponent := TDesignerHelper.FindBindingsScope(Self);
  end;
end;

destructor TBindingList.Destroy();
begin
  FreeAndNil(FAutoUpdateExpression);
  inherited;
end;

procedure TBindingList.DoOnActivating();
begin
  inherited;
  if (FAutoUpdate) then begin
    FAutoUpdateExpression := TBindings.CreateManagedBinding(
      GetSourceScopes(),
      'Items',
      GetComponentScopes(Self),
      'UpdateList()',
      GetOutputConverter(),
      nil,
      []);
  end;
end;

procedure TBindingList.DoOnDeactivated();
begin
  inherited;
  FreeAndNil(FAutoUpdateExpression);
end;

procedure TBindingList.SetAutoUpdate(const AValue: Boolean);
begin
  if (FAutoUpdate <> AValue) then begin
    Active := False;
    FAutoUpdate := AValue;
  end;
end;

procedure TBindingList.UpdateList();
begin
  if (Active) then begin
    FillList();
  end;
end;
{$ENDREGION 'TBindingList'}

end.

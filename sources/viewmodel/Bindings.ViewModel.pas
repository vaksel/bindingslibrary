//  Copyright 2018-2019 Viktor Akselrod
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
unit Bindings.ViewModel;

interface

uses
  System.SysUtils,
  System.Generics.Collections,
  Bindings.Types.Value;

type
  ENotRegisteredException = class(Exception);

  TViewModelClass = class of TCustomViewModel;

  TCustomViewModel = class abstract(TCustomBind)
  strict private
  class var
    FClassesList : TDictionary<String,TViewModelClass>;
    class constructor Create();
    class destructor Destroy();
  public
    {$REGION 'ViewModel registration'}
    class function GetViewModelClassByName(const AName: String): TViewModelClass; static;
    class function IsRegisteredClass(const AName: String): Boolean; static;
    class procedure RegisterViewModelClass(const ASource: TViewModelClass); static;
    class procedure UnregisterViewModelClass(const ASource: TViewModelClass); static;
    class function CreateViewModel(const AName: String): TCustomViewModel; static;
    class function IsRegisteredSelfClass(): Boolean;
    class procedure RegisterSelfClass();
    class procedure UnregisterSelfClass();
    {$ENDREGION 'ViewModel registration'}
  end;

implementation

{ TCustomViewModel }

class constructor TCustomViewModel.Create();
begin
  FClassesList := TDictionary<String,TViewModelClass>.Create();
end;

class destructor TCustomViewModel.Destroy();
begin
  FreeAndNil(FClassesList);
end;

{$REGION 'ViewModel registration'}
class function TCustomViewModel.CreateViewModel(const AName: String): TCustomViewModel;
var
  LClass : TViewModelClass;
begin
  LClass := GetViewModelClassByName(AName);
  if (LClass = nil) then begin
    raise ENotRegisteredException.CreateFmt('ViewModel "%:0s" is not registered. Please call TCustomViewModel.RegisterViewModelClass(%:0s) first.', [AName]);
  end;
  Result := LClass.Create(nil);
end;

class function TCustomViewModel.GetViewModelClassByName(const AName: String): TViewModelClass;
begin
  FClassesList.TryGetValue(AName, Result);
end;

class function TCustomViewModel.IsRegisteredClass(const AName: String): Boolean;
begin
  Result := FClassesList.ContainsKey(AName);
end;

class function TCustomViewModel.IsRegisteredSelfClass(): Boolean;
begin
  Result := IsRegisteredClass(Self.ClassName);
end;

class procedure TCustomViewModel.RegisterSelfClass();
begin
  RegisterViewModelClass(Self);
end;

class procedure TCustomViewModel.RegisterViewModelClass(const ASource: TViewModelClass);
begin
  FClassesList.AddOrSetValue(ASource.ClassName, ASource);
end;

class procedure TCustomViewModel.UnregisterSelfClass();
begin
  UnregisterViewModelClass(Self);
end;

class procedure TCustomViewModel.UnregisterViewModelClass(const ASource: TViewModelClass);
begin
  FClassesList.Remove(ASource.ClassName);
end;
{$ENDREGION 'ViewModel registration'}

end.

//  Copyright 2018 Viktor Akselrod
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.


unit View.MainWindow;

interface

uses
  System.SysUtils,
  System.Types,
  System.UITypes,
  System.Classes,
  System.Rtti,
  System.Bindings.Outputs,
  System.Messaging,
  FMX.Types,
  FMX.Controls,
  FMX.Forms,
  FMX.Graphics,
  FMX.Dialogs,
  FMX.StdCtrls,
  FMX.Controls.Presentation,
  FMX.Edit,
  FMX.Layouts,
  Fmx.Bind.DBEngExt,
  Fmx.Bind.Editors,
  Data.Bind.EngExt,
  Data.Bind.Components,
  Bindings.Expressions.Scope,
  Bindings.Expressions.Components,
  Bindings.Notifications.Handler.FMX,
  View.CustomForm,
  ViewModel.Custom,
  ViewModel.Test;

type
  TViewMainWindow = class(TViewCustomForm)
    Edit1: TEdit;
    TrackBar1: TTrackBar;
    CheckBox1: TCheckBox;
    Button1: TButton;
    BindingString: TBindingControl;
    BindingInt32: TBindingControl;
    BindingBoolean: TBindingControl;
    procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  public
    class procedure Execute(const AViewModel: TViewModelCustom); static;
  end;

var
  ViewMainWindow: TViewMainWindow;

implementation

{$R *.fmx}

{ TViewMainWindow }

class procedure TViewMainWindow.Execute(const AViewModel: TViewModelCustom);
var
  LView : TViewMainWindow;
begin
  LView := TViewMainWindow.Create(nil);
  LView.SetViewModel(AViewModel, False);
  LView.Show();
end;

procedure TViewMainWindow.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  Action := TCloseAction.caFree;
end;

procedure TViewMainWindow.FormCreate(Sender: TObject);
begin
  inherited;
  if (Application.MainForm = nil) then begin
    // Create viewmodel for a first view.
    SetViewModel(TViewModelTest.Create(), True);
  end;
end;

procedure TViewMainWindow.Button1Click(Sender: TObject);
begin
  Execute(ViewModel);
end;

end.



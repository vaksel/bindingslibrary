//  Copyright 2018 Viktor Akselrod
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

unit ViewModel.Test;

interface

uses
  System.SysUtils,
  System.Classes,
  Bindings.Types.Value,
  Bindings.Types.List,
  ViewModel.Custom;

type
  TViewModelTest = class(TViewModelCustom)
  strict private
  var
    FInt32Value   : TBindInt32;
    FStringValue  : TBindString;
    FBoolValue    : TBindBoolean;
  public
    constructor Create(const AOwner: TCustomBind = nil); override;
    destructor Destroy; override;

    property StringValue: TBindString read FStringValue;
    property Int32Value: TBindInt32 read FInt32Value;
    property BoolValue: TBindBoolean read FBoolValue;
  end;

implementation

{ TViewModelTest }

constructor TViewModelTest.Create(const AOwner: TCustomBind);
begin
  inherited;
  FInt32Value := TBindInt32.Create(Self);
  FStringValue := TBindString.Create(Self);
  FBoolValue := TBindBoolean.Create(Self);
end;

destructor TViewModelTest.Destroy();
begin
  FreeAndNil(FInt32Value);
  FreeAndNil(FStringValue);
  FreeAndNil(FBoolValue);
  inherited;
end;

end.

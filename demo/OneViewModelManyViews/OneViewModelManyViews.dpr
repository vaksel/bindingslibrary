program OneViewModelManyViews;

uses
  System.StartUpCopy,
  FMX.Forms,
  View.MainWindow in 'views\View.MainWindow.pas' {ViewMainWindow},
  ViewModel.Custom in '..\share\viewmodels\ViewModel.Custom.pas',
  View.CustomForm in '..\share\views\FMX\View.CustomForm.pas' {ViewCustomForm},
  ViewModel.Test in 'viewmodels\ViewModel.Test.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TViewMainWindow, ViewMainWindow);
  Application.Run;
end.

//  Copyright 2018 Viktor Akselrod
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

unit ViewModel.Custom;

interface

uses
  System.SysUtils,
  System.Classes,
  System.UITypes,
  Bindings.Types.Value;

type
  TShowMessageBoxProc = reference to function(const AMessage: String; const ADialogType: TMsgDlgType;
    const AButtons: TMsgDlgButtons; const ADefaultButton: TMsgDlgBtn; const AHelpCtx: THelpContext): TModalResult;

  TViewModelCustom = class(TCustomBind)
  strict private
  var
    FCaption          : TBindString;
    FEnabled          : TBindBoolean;
    FModified         : TBindBoolean;
    FOnShowMessageBox : TShowMessageBoxProc;
  protected
  type
    TAlertType = (
      Warning,
      Error,
      Information,
      Confirmation
    );
    function CanClose(): Boolean; virtual;
    procedure DoSave(); virtual;
    function ShowQuestion(const AText: String; const AAlertType: TAlertType = TAlertType.Confirmation): Boolean;
    procedure ShowMessage(const AText: String; const AAlertType: TAlertType = TAlertType.Information);
  public
    constructor Create(const AOwner: TCustomBind = nil); override;
    destructor Destroy(); override;

    function Verify(): Boolean; virtual;
    procedure Reset(); virtual;
    function Save(): Boolean; virtual;
    function Close(): Boolean; virtual;
    procedure Refresh(); virtual;

    property Caption: TBindString read FCaption;
    property Enabled: TBindBoolean read FEnabled;
    property Modified: TBindBoolean read FModified;

    property OnShowMessageBox: TShowMessageBoxProc read FOnShowMessageBox write FOnShowMessageBox;
  end;

implementation

{ TViewModelCustom }

constructor TViewModelCustom.Create(const AOwner: TCustomBind);
begin
  inherited Create(AOwner);
  FCaption := TBindString.Create(Self);
  FEnabled := TBindBoolean.CreateEx(Self, True);
  FModified := TBindBoolean.Create(Self);
end;

destructor TViewModelCustom.Destroy();
begin
  FreeAndNil(FModified);
  FreeAndNil(FEnabled);
  FreeAndNil(FCaption);
  inherited;
end;

function TViewModelCustom.CanClose(): Boolean;
begin
  Result := True;
end;

function TViewModelCustom.Close(): Boolean;
begin
  Result := (not Modified.Value) or (CanClose());
end;

procedure TViewModelCustom.DoSave();
begin
end;

procedure TViewModelCustom.Refresh();
begin
end;

procedure TViewModelCustom.Reset();
begin
  FModified.Value := False;
end;

function TViewModelCustom.Save(): Boolean;
begin
  if (Modified.Value) then begin
    Result := Verify();
    if (Result) then begin
      DoSave();
    end;
  end else begin
    Result := True;
  end;
end;

function TViewModelCustom.Verify(): Boolean;
begin
  Result := True;
end;

procedure TViewModelCustom.ShowMessage(const AText: String; const AAlertType: TAlertType);
begin
  FOnShowMessageBox(AText, TMsgDlgType(AAlertType),
    [TMsgDlgBtn.mbOK], TMsgDlgBtn.mbOK, 0);
end;

function TViewModelCustom.ShowQuestion(const AText: String; const AAlertType: TAlertType): Boolean;
begin
  Result := FOnShowMessageBox(AText, TMsgDlgType(AAlertType),
    [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo], TMsgDlgBtn.mbYes, 0) = mrYes;
end;

end.

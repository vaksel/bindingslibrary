object ViewCustomForm: TViewCustomForm
  Left = 0
  Top = 0
  ClientHeight = 291
  ClientWidth = 633
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 120
  TextHeight = 16
  object BindingList: TBindingsList
    Methods = <>
    OutputConverters = <>
    Left = 24
    Top = 32
    object BindCaption: TBindExpression
      Category = 'ViewSettings'
      ControlComponent = Owner
      SourceComponent = ViewModelAdapter
      SourceExpression = 'Caption.Value'
      ControlExpression = 'Caption'
      NotifyOutputs = False
      Direction = dirSourceToControl
    end
    object BindEnabled: TBindExpression
      Category = 'ViewSettings'
      ControlComponent = Owner
      SourceComponent = ViewModelAdapter
      SourceExpression = 'Enabled.Value'
      ControlExpression = 'Enabled'
      NotifyOutputs = False
      Direction = dirSourceToControl
    end
  end
  object ViewModelAdapter: TBindingsScope
    AutoActivate = False
    ScopeMappings = <>
    Left = 24
    Top = 88
  end
  object BindingsControlNotifications: TBindingsControlNotifications
    Left = 24
    Top = 152
  end
end

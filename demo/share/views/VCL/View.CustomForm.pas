//  Copyright 2018 Viktor Akselrod
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

unit View.CustomForm;

interface

uses
  Winapi.Windows,
  Winapi.Messages,
  System.SysUtils,
  System.Variants,
  System.Classes,
  System.Rtti,
  System.Bindings.Outputs,
  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.Bind.DBEngExt,
  Vcl.Bind.Editors,
  Data.Bind.EngExt,
  Data.Bind.Components,
  Bindings.Notifications.Handler.VCL,
  Bindings.Expressions.Scope,
  ViewModel.Custom;

type
  TViewCustomForm = class(TForm)
    BindingList: TBindingsList;
    BindCaption: TBindExpression;
    BindEnabled: TBindExpression;
    ViewModelAdapter: TBindingsScope;
    BindingsControlNotifications: TBindingsControlNotifications;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  strict private
  var
    FViewModel     : TViewModelCustom;
    FOwnsViewModel : Boolean;
    function GetViewModel(): TViewModelCustom;
    function GetHasViewModel(): Boolean;
  protected
    procedure DoInitViewModel(); virtual;
  public
    destructor Destroy(); override;

    procedure SetViewModel(const AViewModel: TViewModelCustom; const AOwnsViewModel: Boolean = False); virtual;

    property ViewModel: TViewModelCustom read GetViewModel;
    function ViewModelAs<T: TViewModelCustom>(): T;
    property HasViewModel: Boolean read GetHasViewModel;
  end;

implementation

{$R *.dfm}

destructor TViewCustomForm.Destroy();
begin
  ViewModelAdapter.Active := False;
  if (FOwnsViewModel) then begin
    FreeAndNil(FViewModel);
  end;
  inherited;
end;

procedure TViewCustomForm.DoInitViewModel();
begin
  // nop.
end;

procedure TViewCustomForm.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  case StripAllFromResult(ModalResult) of
    mrOk, mrYes{, mrContinue}: begin
      CanClose := ViewModel.Save();
    end;
    mrCancel, mrAbort, mrNo, mrClose: begin
      CanClose := ViewModel.Close();
    end;
  end
//  mrRetry
//  mrIgnore
//  mrHelp
//  mrTryAgain
//  mrContinue
end;

function TViewCustomForm.GetHasViewModel(): Boolean;
begin
  Result := FViewModel <> nil;
end;

function TViewCustomForm.GetViewModel(): TViewModelCustom;
begin
  ASSERT(FViewModel <> nil);
  Result := FViewModel;
end;

procedure TViewCustomForm.SetViewModel(const AViewModel: TViewModelCustom; const AOwnsViewModel: Boolean);
begin
  ASSERT(AViewModel <> nil);
  // Auto free ViewModel on destroy view if AOwnsViewModel is True.
  FOwnsViewModel := AOwnsViewModel;
  // Save ViewModel and set callback to show messagebox.
  FViewModel := AViewModel;
  FViewModel.OnShowMessageBox :=
    function(const AMessage: String; const ADialogType: TMsgDlgType; const AButtons: TMsgDlgButtons;
      const ADefaultButton: TMsgDlgBtn; const AHelpCtx: THelpContext): TModalResult
    begin
      Result := MessageDlg(AMessage, ADialogType, AButtons, AHelpCtx, ADefaultButton);
    end;
  // Custom init ViewModel in descendants.
  DoInitViewModel();
  // Set ViewModel as source object to BindScope.
  ViewModelAdapter.DataObject := FViewModel;
  ViewModelAdapter.Active := True;
end;

function TViewCustomForm.ViewModelAs<T>(): T;
begin
  Result := ViewModel as T;
end;

end.

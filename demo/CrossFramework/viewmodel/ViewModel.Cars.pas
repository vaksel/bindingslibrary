//  Copyright 2018 Viktor Akselrod
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

unit ViewModel.Cars;

interface

uses
  System.SysUtils,
  System.Classes,
  System.UITypes,
  System.MultiEvent,
  Bindings.Types.Value,
  Bindings.Types.List,
  Model.Car.Mark,
  Model.Car.Model,
  Model.Car.Generation,
  Model.Car.Serie,
  ViewModel.Custom;

type
  TViewModelCars = class(TViewModelCustom)
  strict private
  var
    FMarkList       : TBindList<TCarMark>;
    FModelList      : TBindList<TCarModel>;
    FGenerationList : TBindList<TCarGeneration>;
    FSerieList      : TBindList<TCarSerie>;
  public
    constructor CreateEx(const AMarks: TCarMarks);
    destructor Destroy(); override;

    property MarkList: TBindList<TCarMark> read FMarkList;
    property ModelList: TBindList<TCarModel> read FModelList;
    property GenerationList: TBindList<TCarGeneration> read FGenerationList;
    property SerieList: TBindList<TCarSerie> read FSerieList;
  end;

implementation

{ TViewModelCars }

constructor TViewModelCars.CreateEx(const AMarks: TCarMarks);
var
  LMark : TCarMark;
begin
  Create();
  ASSERT(AMarks <> nil);
  Caption.Value := 'Cars list';
  {$REGION 'MarkList'}
  FMarkList := TBindList<TCarMark>.CreateEx(Self, False);
  FMarkList.OnChange :=
    procedure (Sender: TObject; const E: TEventArgs)
    var
      LMark   : TCarMark;
      LModel  : TCarModel;
    begin
      BeginUpdate();
      try
        LMark := nil;
        if (FMarkList.HasValue) then begin
          LMark := FMarkList.Value;
        end;
        FModelList.Clear();
        if (LMark <> nil) then begin
          for LModel in LMark.Models do begin
            FModelList.Items.Add(LModel);
          end;
        end;
      finally
        EndUpdate();
      end;
    end;
  {$ENDREGION 'MarkList'}
  {$REGION 'ModelList'}
  FModelList := TBindList<TCarModel>.CreateEx(Self, False);
  FModelList.OnChange :=
    procedure (Sender: TObject; const E: TEventArgs)
    var
      LModel       : TCarModel;
      LGeneration  : TCarGeneration;
    begin
      BeginUpdate();
      try
        LModel := nil;
        if (FModelList.HasValue) then begin
          LModel := FModelList.Value;
        end;
        FGenerationList.Clear();
        if (LModel <> nil) then begin
          for LGeneration in LModel.Generations do begin
            FGenerationList.Items.Add(LGeneration);
          end;
        end;
      finally
        EndUpdate();
      end;
    end;
  {$ENDREGION 'ModelList'}
  {$REGION 'GenerationList'}
  FGenerationList := TBindList<TCarGeneration>.CreateEx(Self, False);
  FGenerationList.OnChange :=
    procedure (Sender: TObject; const E: TEventArgs)
    var
      LGeneration   : TCarGeneration;
      LSerie        : TCarSerie;
    begin
      BeginUpdate();
      try
        LGeneration := nil;
        if (FGenerationList.HasValue) then begin
          LGeneration := FGenerationList.Value;
        end;
        FSerieList.Clear();
        if (LGeneration <> nil) then begin
          for LSerie in LGeneration.Series do begin
            FSerieList.Items.Add(LSerie);
          end;
        end;
      finally
        EndUpdate();
      end;
    end;
  {$ENDREGION 'GenerationList'}
  FSerieList := TBindList<TCarSerie>.CreateEx(Self, False);
  BeginUpdate();
  try
    for LMark in AMarks do begin
      FMarkList.Items.Add(LMark);
    end;
  finally
    EndUpdate();
  end;
end;

destructor TViewModelCars.Destroy();
begin
  FreeAndNil(FMarkList);
  FreeAndNil(FModelList);
  FreeAndNil(FGenerationList);
  FreeAndNil(FSerieList);
  inherited;
end;

end.

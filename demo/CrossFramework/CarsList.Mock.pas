//  Copyright 2018 Viktor Akselrod
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

unit CarsList.Mock;

interface

uses
  System.SysUtils,
  Model.Car.Mark,
  Model.Car.Model,
  Model.Car.Generation,
  Model.Car.Serie;

type
  TCarsListMock = class abstract(TObject)
  strict private
  class var
    FMarks : TCarMarks;
    class constructor Create();
    class destructor Destroy();
  public
    class property Marks: TCarMarks read FMarks;
  end;

implementation

{ TCarsListMock }

class constructor TCarsListMock.Create();
var
  LMark           : TCarMark;
  LModel          : TCarModel;
  LCarGeneration  : TCarGeneration;
  LSerie          : TCarSerie;
begin
  FMarks := TCarMarks.Create();

  LMark := TCarMark.Create('Honda');
  FMarks.Add(LMark);

  LModel := TCarModel.Create('Accord');
  LMark.Models.Add(LModel);

  LCarGeneration := TCarGeneration.Create('9 ���������');
  LModel.Generations.Add(LCarGeneration);
  LCarGeneration.Series.AddRange([TCarSerie.Create('�����'), TCarSerie.Create('����')]);

  LCarGeneration := TCarGeneration.Create('7 ���������');
  LModel.Generations.Add(LCarGeneration);
  LCarGeneration.Series.AddRange([TCarSerie.Create('US-spec ����� 4-��.'), TCarSerie.Create('US-spec ����'), TCarSerie.Create('����� 4-��.'), TCarSerie.Create('���������')]);

  LCarGeneration := TCarGeneration.Create('3 ���������');
  LModel.Generations.Add(LCarGeneration);
  LCarGeneration.Series.AddRange([TCarSerie.Create('US-spec ����� 4-��.'), TCarSerie.Create('US-spec �������'), TCarSerie.Create('�����'), TCarSerie.Create('����'), TCarSerie.Create('���������')]);

  LModel := TCarModel.Create('Airwave');
  LMark.Models.Add(LModel);

  LCarGeneration := TCarGeneration.Create('1 ��������� [2005 - 2008]');
  LModel.Generations.Add(LCarGeneration);
  LCarGeneration.Series.AddRange([TCarSerie.Create('���������')]);

  LCarGeneration := TCarGeneration.Create('1 ��������� [����������] [2008 - 2010]');
  LModel.Generations.Add(LCarGeneration);
  LCarGeneration.Series.AddRange([TCarSerie.Create('���������')]);

  LModel := TCarModel.Create('Ascot');
  LMark.Models.Add(LModel);

  LCarGeneration := TCarGeneration.Create('CB [1989 - 1993]]');
  LModel.Generations.Add(LCarGeneration);
  LCarGeneration.Series.AddRange([TCarSerie.Create('�����')]);

  LCarGeneration := TCarGeneration.Create('CE [1993 - 1997]');
  LModel.Generations.Add(LCarGeneration);
  LCarGeneration.Series.AddRange([TCarSerie.Create('�����')]);

  LModel := TCarModel.Create('Avancier');
  LMark.Models.Add(LModel);

  LCarGeneration := TCarGeneration.Create('1 ��������� [1999 - 2003]');
  LModel.Generations.Add(LCarGeneration);
  LCarGeneration.Series.AddRange([TCarSerie.Create('Nouvelle Vague ��������� 5-��.'), TCarSerie.Create('��������� 5-��.')]);

  LMark := TCarMark.Create('Infiniti');
  FMarks.Add(LMark);

  LModel := TCarModel.Create('QX80');
  LMark.Models.Add(LModel);

  LCarGeneration := TCarGeneration.Create('Z62 [2013 - 2014]');
  LModel.Generations.Add(LCarGeneration);
  LCarGeneration.Series.AddRange([TCarSerie.Create('�����������')]);

  LCarGeneration := TCarGeneration.Create('Z62 [2-� ����������] [2017 - 2018]');
  LModel.Generations.Add(LCarGeneration);
  LCarGeneration.Series.AddRange([TCarSerie.Create('�����������')]);

  LCarGeneration := TCarGeneration.Create('Z62 [����������] [2014 - 2018]');
  LModel.Generations.Add(LCarGeneration);
  LCarGeneration.Series.AddRange([TCarSerie.Create('�����������')]);

  LModel := TCarModel.Create('QX70');
  LMark.Models.Add(LModel);

  LCarGeneration := TCarGeneration.Create('S51 [2013 - 2018]');
  LModel.Generations.Add(LCarGeneration);
  LCarGeneration.Series.AddRange([TCarSerie.Create('���������')]);
end;

class destructor TCarsListMock.Destroy();
begin
  FreeAndNil(FMarks);
end;

end.

//  Copyright 2018 Viktor Akselrod
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

unit Model.Car.Generation;

interface

uses
  System.SysUtils,
  System.Generics.Collections,
  Model.Car.Serie;

type
  TCarSeries = TObjectList<TCarSerie>;

  TCarGeneration = class(TObject)
  strict private
  var
    FName    : String;
    FSeries  : TCarSeries;
  public
    constructor Create(const AName: String);
    destructor Destroy(); override;

    property Name: String read FName write FName;
    property Series: TCarSeries read FSeries;
  end;

implementation

{ TCarGeneration }

constructor TCarGeneration.Create(const AName: String);
begin
  inherited Create();
  FName := AName;
  FSeries := TCarSeries.Create();
end;

destructor TCarGeneration.Destroy();
begin
  FreeAndNil(FSeries);
  inherited;
end;

end.

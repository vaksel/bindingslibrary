//  Copyright 2018 Viktor Akselrod
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

unit Model.Car.Mark;

interface

uses
  System.SysUtils,
  System.Generics.Collections,
  Model.Car.Model;

type
  TCarMark = class;
  TCarMarks  = TObjectList<TCarMark>;
  TCarModels = TObjectList<TCarModel>;

  TCarMark = class(TObject)
  strict private
  var
    FName    : String;
    FModels  : TCarModels;
  public
    constructor Create(const AName: String);
    destructor Destroy(); override;

    property Name: String read FName write FName;
    property Models: TCarModels read FModels;
  end;

implementation

{ TCarMark }

constructor TCarMark.Create(const AName: String);
begin
  inherited Create();
  FName := AName;
  FModels := TCarModels.Create();
end;

destructor TCarMark.Destroy();
begin
  FreeAndNil(FModels);
  inherited;
end;

end.

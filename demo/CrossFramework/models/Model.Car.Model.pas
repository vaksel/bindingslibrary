//  Copyright 2018 Viktor Akselrod
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

unit Model.Car.Model;

interface

uses
  System.SysUtils,
  System.Generics.Collections,
  Model.Car.Generation;

type
  TCarGenerations = TObjectList<TCarGeneration>;

  TCarModel = class(TObject)
  strict private
  var
    FName    : String;
    FSeries  : TCarGenerations;
  public
    constructor Create(const AName: String);
    destructor Destroy(); override;

    property Name: String read FName write FName;
    property Generations: TCarGenerations read FSeries;
  end;

implementation

{ TCarModel }

constructor TCarModel.Create(const AName: String);
begin
  inherited Create();
  FName := AName;
  FSeries := TCarGenerations.Create();
end;

destructor TCarModel.Destroy();
begin
  FreeAndNil(FSeries);
  inherited;
end;

end.

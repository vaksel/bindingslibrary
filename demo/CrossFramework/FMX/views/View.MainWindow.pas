//  Copyright 2018 Viktor Akselrod
//
//  Licensed under the Apache License, Version 2.0 (the "License");
//  you may not use this file except in compliance with the License.
//  You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.

unit View.MainWindow;

interface

uses
  System.SysUtils,
  System.Types,
  System.UITypes,
  System.Classes,
  System.Variants,
  System.Rtti,
  System.Bindings.Outputs,
  FMX.Types,
  FMX.Controls,
  FMX.Forms,
  FMX.Graphics,
  FMX.Dialogs,
  FMX.ListBox,
  FMX.Controls.Presentation,
  FMX.StdCtrls,
  FMX.Layouts,
  Fmx.Bind.DBEngExt,
  Fmx.Bind.Editors,
  View.CustomForm,
  Data.Bind.EngExt,
  Data.Bind.Components,
  Bindings.Expressions.Scope,
  Bindings.Notifications.Handler.FMX,
  Bindings.Expressions.Components,
  ViewModel.Cars,
  CarsList.Mock;

type
  TViewMainWindow = class(TViewCustomForm)
    lblMark: TLabel;
    cbxMark: TComboBox;
    lblModel: TLabel;
    cbxModel: TComboBox;
    lblGeneration: TLabel;
    cbxGeneration: TComboBox;
    lblSerie: TLabel;
    cbxSerie: TComboBox;
    BindingMarkListFill: TBindingList;
    BindingMarkList: TBindingControl;
    BindingModelListFill: TBindingList;
    BindingModelList: TBindingControl;
    BindingGenerationListFill: TBindingList;
    BindingGenerationList: TBindingControl;
    BindingSerieListFill: TBindingList;
    BindingSerieList: TBindingControl;
  public
    constructor Create(AOwner: TComponent); override;
  end;

var
  ViewMainWindow: TViewMainWindow;

implementation

{$R *.fmx}

{ TViewMainWindow }

constructor TViewMainWindow.Create(AOwner: TComponent);
begin
  inherited;
  SetViewModel(TViewModelCars.CreateEx(TCarsListMock.Marks), True);
end;

end.

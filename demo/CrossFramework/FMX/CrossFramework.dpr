program CrossFramework;

uses
  System.StartUpCopy,
  FMX.Forms,
  View.MainWindow in 'views\View.MainWindow.pas' {ViewMainWindow},
  View.CustomForm in '..\..\share\views\FMX\View.CustomForm.pas' {ViewCustomForm},
  CarsList.Mock in '..\CarsList.Mock.pas',
  Model.Car.Generation in '..\models\Model.Car.Generation.pas',
  Model.Car.Mark in '..\models\Model.Car.Mark.pas',
  Model.Car.Model in '..\models\Model.Car.Model.pas',
  Model.Car.Serie in '..\models\Model.Car.Serie.pas',
  ViewModel.Custom in '..\..\share\viewmodels\ViewModel.Custom.pas',
  ViewModel.Cars in '..\viewmodel\ViewModel.Cars.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TViewMainWindow, ViewMainWindow);
  Application.Run;
end.

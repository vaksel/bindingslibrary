inherited ViewMainWindow: TViewMainWindow
  ActiveControl = cbxMark
  BorderStyle = bsDialog
  Caption = 'ViewMainWindow'
  ClientHeight = 249
  ClientWidth = 358
  Padding.Left = 20
  Padding.Top = 20
  Padding.Right = 20
  Padding.Bottom = 20
  Position = poScreenCenter
  ExplicitWidth = 364
  ExplicitHeight = 284
  PixelsPerInch = 120
  TextHeight = 16
  object lblMark: TLabel [0]
    Left = 20
    Top = 20
    Width = 318
    Height = 16
    Align = alTop
    Caption = 'Mark'
    ExplicitWidth = 28
  end
  object lblModel: TLabel [1]
    AlignWithMargins = True
    Left = 20
    Top = 72
    Width = 318
    Height = 16
    Margins.Left = 0
    Margins.Top = 10
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alTop
    Caption = 'Model'
    ExplicitWidth = 34
  end
  object lblGeneration: TLabel [2]
    AlignWithMargins = True
    Left = 20
    Top = 124
    Width = 318
    Height = 16
    Margins.Left = 0
    Margins.Top = 10
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alTop
    Caption = 'Generation'
    ExplicitWidth = 62
  end
  object lblSerie: TLabel [3]
    AlignWithMargins = True
    Left = 20
    Top = 176
    Width = 318
    Height = 16
    Margins.Left = 0
    Margins.Top = 10
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alTop
    Caption = 'Serie'
    ExplicitWidth = 30
  end
  object cbxMark: TComboBox [4]
    AlignWithMargins = True
    Left = 20
    Top = 38
    Width = 318
    Height = 24
    Margins.Left = 0
    Margins.Top = 2
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alTop
    Style = csDropDownList
    TabOrder = 0
  end
  object cbxModel: TComboBox [5]
    AlignWithMargins = True
    Left = 20
    Top = 90
    Width = 318
    Height = 24
    Margins.Left = 0
    Margins.Top = 2
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alTop
    Style = csDropDownList
    TabOrder = 1
  end
  object cbxGeneration: TComboBox [6]
    AlignWithMargins = True
    Left = 20
    Top = 142
    Width = 318
    Height = 24
    Margins.Left = 0
    Margins.Top = 2
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alTop
    Style = csDropDownList
    TabOrder = 2
  end
  object cbxSerie: TComboBox [7]
    AlignWithMargins = True
    Left = 20
    Top = 194
    Width = 318
    Height = 24
    Margins.Left = 0
    Margins.Top = 2
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alTop
    Style = csDropDownList
    TabOrder = 3
  end
  inherited BindingList: TBindingsList
    inherited BindCaption: TBindExpression
      ControlComponent = Owner
    end
    inherited BindEnabled: TBindExpression
      ControlComponent = Owner
    end
    object BindingMarkListFill: TBindingList
      Category = 'BindingsLibrary'
      ControlComponent = cbxMark
      SourceComponent = ViewModelAdapter
      SourceMemberName = 'MarkList'
      FormatExpressions = <
        item
          ControlExpression = 'Text'
          SourceExpression = 'Current.Name'
        end>
      FormatControlExpressions = <>
      ClearControlExpressions = <>
    end
    object BindingMarkList: TBindingControl
      Category = 'BindingsLibrary'
      ControlComponent = cbxMark
      SourceComponent = ViewModelAdapter
      ControlNotification.PropertyName = 'ItemIndex'
      SourceExpression = 'MarkList.ItemIndex.Value'
      ControlExpression = 'ItemIndex'
    end
    object BindingModelListFill: TBindingList
      Category = 'BindingsLibrary'
      ControlComponent = cbxModel
      SourceComponent = ViewModelAdapter
      SourceMemberName = 'ModelList'
      FormatExpressions = <
        item
          ControlExpression = 'Text'
          SourceExpression = 'Current.Name'
        end>
      FormatControlExpressions = <>
      ClearControlExpressions = <>
    end
    object BindingModelList: TBindingControl
      Category = 'BindingsLibrary'
      ControlComponent = cbxModel
      SourceComponent = ViewModelAdapter
      ControlNotification.PropertyName = 'ItemIndex'
      SourceExpression = 'ModelList.ItemIndex.Value'
      ControlExpression = 'ItemIndex'
    end
    object BindingGenerationListFill: TBindingList
      Category = 'BindingsLibrary'
      ControlComponent = cbxGeneration
      SourceComponent = ViewModelAdapter
      SourceMemberName = 'GenerationList'
      FormatExpressions = <
        item
          ControlExpression = 'Text'
          SourceExpression = 'Current.Name'
        end>
      FormatControlExpressions = <>
      ClearControlExpressions = <>
    end
    object BindingGenerationList: TBindingControl
      Category = 'BindingsLibrary'
      ControlComponent = cbxGeneration
      SourceComponent = ViewModelAdapter
      ControlNotification.PropertyName = 'ItemIndex'
      SourceExpression = 'GenerationList.ItemIndex.Value'
      ControlExpression = 'ItemIndex'
    end
    object BindingSerieListFill: TBindingList
      Category = 'BindingsLibrary'
      ControlComponent = cbxSerie
      SourceComponent = ViewModelAdapter
      SourceMemberName = 'SerieList'
      FormatExpressions = <
        item
          ControlExpression = 'Text'
          SourceExpression = 'Current.Name'
        end>
      FormatControlExpressions = <>
      ClearControlExpressions = <>
    end
    object BindingSerieList: TBindingControl
      Category = 'BindingsLibrary'
      ControlComponent = cbxSerie
      SourceComponent = ViewModelAdapter
      ControlNotification.PropertyName = 'ItemIndex'
      SourceExpression = 'SerieList.ItemIndex.Value'
      ControlExpression = 'ItemIndex'
    end
  end
end

program CrossFramework;

uses
  Vcl.Forms,
  View.MainWindow in 'views\View.MainWindow.pas' {ViewMainWindow},
  ViewModel.Custom in '..\..\share\viewmodels\ViewModel.Custom.pas',
  View.CustomForm in '..\..\share\views\VCL\View.CustomForm.pas' {ViewCustomForm},
  ViewModel.Cars in '..\viewmodel\ViewModel.Cars.pas',
  CarsList.Mock in '..\CarsList.Mock.pas',
  Model.Car.Generation in '..\models\Model.Car.Generation.pas',
  Model.Car.Mark in '..\models\Model.Car.Mark.pas',
  Model.Car.Model in '..\models\Model.Car.Model.pas',
  Model.Car.Serie in '..\models\Model.Car.Serie.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TViewMainWindow, ViewMainWindow);
  Application.Run;
end.

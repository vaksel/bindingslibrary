unit ViewModel.ContactList;

interface

uses
  System.SysUtils,
  System.MultiEvent,
  System.Generics.Collections,
  Bindings.Types.Value,
  Bindings.Types.List,
  Bindings.Types.Command,
  Bindings.Types.Control,
  ViewModel.Custom,
  Model.Contacts,
  Model.Contact;

type
  TViewModelContactList = class(TViewModelCustom)
  strict private
  var
    FContacts           : TContacts;
    FContactList        : TBindList<TContact>;
    FFilter             : TBindString;
    FCmdAddContact      : TBindCommand;
    FCmdViewContact     : TBindControl<TBindCommand>;
    FCmdDeleteContact   : TBindControl<TBindCommand>;
    FOnContactView      : TContactProc;
    FOnContactAdd       : TContactProc;
  public
    constructor CreateEx(const AContacts: TContacts);
    destructor Destroy(); override;

    procedure Refresh(); override;

    property ContactList: TBindList<TContact> read FContactList;
    property Filter: TBindString read FFilter;

    property CmdAddContact: TBindCommand read FCmdAddContact;
    property CmdViewContact: TBindControl<TBindCommand> read FCmdViewContact;
    property CmdDeleteContact: TBindControl<TBindCommand> read FCmdDeleteContact;

    property OnContactView: TContactProc read FOnContactView write FOnContactView;
    property OnContactAdd: TContactProc read FOnContactAdd write FOnContactAdd;
  end;

implementation

{ TViewModelContactList }

constructor TViewModelContactList.CreateEx(const AContacts: TContacts);
begin
  Create();
  FContacts := AContacts;
  FContactList := TBindList<TContact>.CreateEx(Self, False);
  FContactList.OnChange :=
    procedure (Sender: TObject; const E: TEventArgs)
    begin
      FCmdViewContact.Enabled.Value := FContactList.Value <> nil;
      FCmdDeleteContact.Enabled.Value := FContactList.Value <> nil;
    end;
  FFilter := TBindString.Create(Self);
  FFilter.OnChange :=
    procedure (Sender: TObject; const E: TEventArgs)
    begin
      Refresh();
    end;
  FCmdAddContact := TBindCommand.Create(Self);
  FCmdAddContact.Proc :=
    procedure ()
    var
      LContact : TContact;
      LState   : TContactState;
    begin
      if (not Assigned(FOnContactAdd)) then begin
        Exit;
      end;
      LContact := TContact.Create();
      try
        LState := FOnContactAdd(LContact);
        if (LState = TContactState.Changed) then begin
          FContacts.Add(LContact);
          LContact := nil;
          Refresh();
        end;
      finally
        FreeAndNil(LContact);
      end;
    end;
  FCmdViewContact := TBindControl<TBindCommand>.CreateEx(Self);
  FCmdViewContact.Enabled.Value := False;
  FCmdViewContact.Data.Proc :=
    procedure ()
    var
      LContact : TContact;
      LState   : TContactState;
    begin
      if (not Assigned(FOnContactView)) then begin
        Exit;
      end;
      LContact := FContactList.Value;
      if (LContact = nil) then begin
        Exit;
      end;
      LState := FOnContactView(LContact);
      if (LState = TContactState.Deleted) then begin
        FContacts.Remove(LContact);
        Refresh();
      end else if (LState = TContactState.Changed) then begin
        FContactList.Update();
      end;
    end;
  FCmdDeleteContact := TBindControl<TBindCommand>.CreateEx(Self);
  FCmdDeleteContact.Enabled.Value := False;
  FCmdDeleteContact.Data.Proc :=
    procedure ()
    var
      LContact : TContact;
    begin
      LContact := FContactList.Value;
      if (LContact = nil) then begin
        Exit;
      end;
      if (ShowQuestion('Are you sure delete this contact?')) then begin
        FContacts.Remove(LContact);
        Refresh();
      end;
    end;
  Refresh();
end;

destructor TViewModelContactList.Destroy();
begin
  FreeAndNil(FCmdAddContact);
  FreeAndNil(FCmdDeleteContact);
  FreeAndNil(FCmdViewContact);
  FreeAndNil(FFilter);
  FreeAndNil(FContactList);
  inherited;
end;

procedure TViewModelContactList.Refresh();
var
  LContact : TContact;
  LAllow   : Boolean;
  LFilter: string;
begin
  FContactList.BeginUpdate();
  try
    FContactList.Clear();
    if (FFilter.Value.IsEmpty) then begin
      FContactList.Items.AddRange(FContacts);
    end else begin
      LFilter := FFilter.Value.ToLower;
      for LContact in FContacts do begin
        LAllow := (LContact.FirstName.ToLower.Contains(LFilter))
          or (LContact.LastName.ToLower.Contains(LFilter))
          or (LContact.PhoneMobile.Contains(FFilter.Value));
        if (LAllow) then begin
          FContactList.Items.Add(LContact);
        end;
      end;
    end;
  finally
    FContactList.EndUpdate();
  end;
end;

end.

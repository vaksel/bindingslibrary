unit ViewModel.Contact;

interface

uses
  System.SysUtils,
  System.Classes,
  System.RegularExpressions,
  System.MultiEvent,
  System.UITypes,
  Bindings.Types.Value,
  Bindings.Types.Command,
  Bindings.Types.Control,
  ViewModel.Custom,
  Model.Photo,
  Model.Contact;

type
  TUpdatePhotoProc = reference to function (const APhoto: TPhoto): Boolean;

  TWorkMode = (
    Create,
    Edit,
    View
  );

  TViewModelContact = class(TViewModelCustom)
  strict private
  var
    FContact        : TContact;
    FWorkMode       : TWorkMode;
    FStates         : set of TContactState;
    FPhoneWork      : TBindValidatedString;
    FLastName       : TBindString;
    FWork           : TBindString;
    FPhoneMobile    : TBindValidatedString;
    FFirstName      : TBindString;
    FNote           : TBindString;
    FBirthday       : TBindDate;
    FPhoto          : TBindValue<TPhoto>;
    FCmdEdit        : TBindControl<TBindCommand>;
    FCmdDelete      : TBindControl<TBindCommand>;
    FCmdChangePhoto : TBindCommand;
    FOnEdit         : TContactProc;
    FOnUpdatePhoto  : TUpdatePhotoProc;
    function ValidatePhoneNumber(const ASource: String): Boolean;
    function GetContactState(): TContactState;
  protected
    procedure DoSave(); override;
    function CanClose(): Boolean; override;
  public
    constructor CreateEx(const AContact: TContact; const AWorkMode: TWorkMode);
    destructor Destroy(); override;

    function Verify(): Boolean; override;

    property State: TContactState read GetContactState;
    property FirstName: TBindString read FFirstName;
    property LastName: TBindString read FLastName;
    property PhoneMobile: TBindValidatedString read FPhoneMobile;
    property PhoneWork: TBindValidatedString read FPhoneWork;
    property Work: TBindString read FWork;
    property Note: TBindString read FNote;
    property Birthday: TBindDate read FBirthday;
    property Photo: TBindValue<TPhoto> read FPhoto;
    property CmdEdit: TBindControl<TBindCommand> read FCmdEdit;
    property CmdDelete: TBindControl<TBindCommand> read FCmdDelete;
    property CmdChangePhoto: TBindCommand read FCmdChangePhoto;

    property OnEdit: TContactProc read FOnEdit write FOnEdit;
    property OnUpdatePhoto: TUpdatePhotoProc read FOnUpdatePhoto write FOnUpdatePhoto;
  end;

implementation

{ TViewModelContact }

constructor TViewModelContact.CreateEx(const AContact: TContact; const AWorkMode: TWorkMode);
begin
  Create();
  FContact := AContact;
  FWorkMode := AWorkMode;
  case FWorkMode of
    TWorkMode.Create: begin
      Caption.Value := 'Create contact';
    end;
    TWorkMode.Edit: begin
      Caption.Value := 'Edit contact';
    end;
    TWorkMode.View: begin
     Caption.Value := 'View contact';
    end else begin
      ASSERT(False);
    end;
  end;
  FFirstName := TBindString.CreateEx(Self, FContact.FirstName);
  FFirstName.OnChange :=
    procedure (Sender: TObject; const E: TEventArgs)
    begin
      Include(FStates, TContactState.Changed);
      Modified.Value := True;
    end;
  FLastName := TBindString.CreateEx(Self, FContact.LastName);
  FLastName.OnChange := FFirstName.OnChange;
  FPhoneMobile := TBindValidatedString.CreateEx(Self, FContact.PhoneMobile);
  FPhoneMobile.OnChange := FFirstName.OnChange;
  FPhoneMobile.OnValidate :=
    procedure (Sender: TObject; const E: TValidateEventArgs<String>)
    begin
      E.Success := ValidatePhoneNumber(E.NewValue);
      if (not E.Success) then begin
        E.Error := 'Wrong number format!';
      end;
    end;
  FPhoneWork := TBindValidatedString.CreateEx(Self, FContact.PhoneWork);
  FPhoneWork.OnChange := FFirstName.OnChange;
  FPhoneWork.OnValidate := FPhoneMobile.OnValidate;
  FWork := TBindString.CreateEx(Self, FContact.Work);
  FWork.OnChange := FFirstName.OnChange;
  FBirthday := TBindDate.CreateEx(Self, FContact.Birthday);
  FBirthday.OnChange := FFirstName.OnChange;
  FNote := TBindString.CreateEx(Self, FContact.Note);
  FNote.OnChange := FFirstName.OnChange;
  FPhoto := TBindValue<TPhoto>.CreateEx(Self, TPhoto.Create());
  FPhoto.Value.Assign(FContact.Photo);
  FPhoto.OnChange := FFirstName.OnChange;
  FCmdEdit := TBindControl<TBindCommand>.CreateEx(Self);
  FCmdEdit.Visible.Value := FWorkMode = TWorkMode.View;
  FCmdEdit.Data.Proc :=
    procedure ()
    begin
      if (Assigned(FOnEdit)) then begin
        Include(FStates, FOnEdit(FContact));
      end;
    end;
  FCmdDelete := TBindControl<TBindCommand>.CreateEx(Self);
  FCmdDelete.Visible.Value := FWorkMode in [TWorkMode.Edit, TWorkMode.View];
  FCmdDelete.Data.Proc :=
    procedure ()
    begin
      Include(FStates, TContactState.Deleted);
      Modified.Value := True;
    end;
  FCmdChangePhoto := TBindCommand.Create(Self);
  FCmdChangePhoto.Proc :=
    procedure ()
    var
      LPhoto : TPhoto;
    begin
      if (not Assigned(FOnUpdatePhoto)) then begin
        Exit;
      end;
      LPhoto := TPhoto.Create();
      try
        if (FOnUpdatePhoto(LPhoto)) then begin
          FPhoto.Value := LPhoto;
          LPhoto := nil;
        end;
      finally
        FreeAndNil(LPhoto);
      end;
    end;
end;

destructor TViewModelContact.Destroy();
begin
  FPhoto.Value.Free();
  FreeAndNil(FCmdChangePhoto);
  FreeAndNil(FCmdDelete);
  FreeAndNil(FCmdEdit);
  FreeAndNil(FNote);
  FreeAndNil(FPhoto);
  FreeAndNil(FFirstName);
  FreeAndNil(FLastName);
  FreeAndNil(FPhoneMobile);
  FreeAndNil(FWork);
  FreeAndNil(FPhoneWork);
  FreeAndNil(FBirthday);
  inherited;
end;

function TViewModelContact.CanClose(): Boolean;
begin
  Result := True;
  if (FWorkMode <> TWorkMode.View) then begin
    Result := ShowQuestion('Do you want close dialog without saving?');
  end;
  if (Result) then begin
    FStates := [];
  end;
end;

procedure TViewModelContact.DoSave();
begin
  if (TContactState.Deleted in FStates) then begin
    Exit;
  end;
  FContact.FirstName := FFirstName.Value;
  FContact.LastName := FLastName.Value;
  FContact.PhoneMobile := FPhoneMobile.Value;
  FContact.PhoneWork := FPhoneWork.Value;
  FContact.Work := FWork.Value;
  FContact.Birthday := FBirthday.Value;
  FContact.Note := FNote.Value;
  FContact.Photo := FPhoto.Value;
end;

function TViewModelContact.Verify(): Boolean;
begin
  // Delete contact.
  Result := True;
  if (FWorkMode in [TWorkMode.Edit, TWorkMode.View]) then begin
    if (TContactState.Deleted in FStates) then begin
      Result := ShowQuestion('Are you sure delete this contact?');
      if (not Result) then begin
        Exclude(FStates, TContactState.Deleted);
      end;
      Exit;
    end;
  end;
  // Modify contact.
  if (FWorkMode in [TWorkMode.Edit, TWorkMode.Create]) then begin
    if (TContactState.Changed in FStates) then begin
      if (String.IsNullOrWhiteSpace(FFirstName.Value + FLastName.Value)) then begin
        ShowMessage('First name or last name must be filled.');
        Exit(False);
      end;
      if (not ValidatePhoneNumber(FPhoneMobile.Value)) then begin
        ShowMessage('Wrong mobile phone format.');
        Exit(False);
      end;
      if (not ValidatePhoneNumber(FPhoneWork.Value)) then begin
        ShowMessage('Wrong work phone format.');
        Exit(False);
      end;
    end;
  end;
end;

function TViewModelContact.GetContactState(): TContactState;
begin
  if (TContactState.Deleted in FStates) then begin
    Result := TContactState.Deleted;
  end else if (TContactState.Changed in FStates) then begin
    Result := TContactState.Changed;
  end else begin
    Result := TContactState.None;
  end;
end;

function TViewModelContact.ValidatePhoneNumber(const ASource: String): Boolean;
begin
  Result := TRegEx.IsMatch(ASource, '^\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})$');
end;

end.

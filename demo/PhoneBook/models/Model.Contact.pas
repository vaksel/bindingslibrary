unit Model.Contact;

interface

uses
  System.SysUtils,
  System.Classes,
  Model.Photo;

type
  TContact = class(TObject)
  strict private
  var
    FFirstName    : String;
    FLastName     : String;
    FPhoneWork    : String;
    FPhoneMobile  : String;
    FWork         : String;
    FNote         : String;
    FBirthday     : TDate;
    FPhoto        : TPhoto;
    procedure SetPhoto(const AValue: TPhoto);
  public
    constructor Create();
    destructor Destroy(); override;

    property FirstName: String read FFirstName write FFirstName;
    property LastName: String read FLastName write FLastName;
    property Work: String read FWork write FWork;
    property PhoneMobile: String read FPhoneMobile write FPhoneMobile;
    property PhoneWork: String read FPhoneWork write FPhoneWork;
    property Birthday: TDate read FBirthday write FBirthday;
    property Photo: TPhoto read FPhoto write SetPhoto;
    property Note: String read FNote write FNote;
  end;

  TContactState = (
    None,
    Changed,
    Deleted
  );

  TContactProc = reference to function (const AContact: TContact): TContactState;

implementation

{ TContact }

constructor TContact.Create();
begin
  inherited;
  FPhoto := TPhoto.Create();
end;

destructor TContact.Destroy();
begin
  FreeAndNil(FPhoto);
  inherited;
end;

procedure TContact.SetPhoto(const AValue: TPhoto);
begin
  FPhoto.Assign(AValue);
end;

end.

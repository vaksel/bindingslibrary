unit Model.Photo;

interface

uses
{$IFDEF DUNITX}
  System.Classes;
{$ENDIF}
{$IFDEF FMX}
  FMX.Graphics;
{$ENDIF}

type
{$IFDEF DUNITX}
  TPhoto = TPersistent;
{$ENDIF}
{$IFDEF FMX}
  TPhoto = FMX.Graphics.TBitmap;
{$ENDIF}

implementation

end.

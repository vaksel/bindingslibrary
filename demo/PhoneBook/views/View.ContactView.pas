unit View.ContactView;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Types,
  System.UITypes,
  System.Rtti,
  System.ImageList,
  System.Bindings.Outputs,
  FMX.Types,
  FMX.Graphics,
  FMX.Controls,
  FMX.Forms,
  FMX.Dialogs,
  FMX.StdCtrls,
  FMX.Layouts,
  FMX.Controls.Presentation,
  FMX.ScrollBox,
  FMX.Memo,
  FMX.Ani,
  FMX.Objects,
  FMX.DateTimeCtrls,
  FMX.Edit,
  FMX.ImgList,
  Fmx.Bind.DBEngExt,
  Fmx.Bind.Editors,
  Data.Bind.Components,
  Data.Bind.EngExt,
  View.CustomForm,
  Bindings.Expressions.Components,
  Bindings.Expressions.Scope,
  Bindings.Notifications.Handler.FMX,
  Model.Contact,
  View.ContactEdit,
  ViewModel.Contact;

type
  TViewContactView = class(TViewContactEdit)
    lblName: TLabel;
    BindName: TBindExpression;
    aniEdit: TFloatAnimation;
    BindCmdEdit: TBindingCommand;
    btnEdit: TSpeedButton;
  protected
    procedure DoInitViewModel(); override;
  end;

implementation

{$R *.fmx}

{ TViewContactView }

procedure TViewContactView.DoInitViewModel();
begin
  inherited;
  ViewModelAs<TViewModelContact>.OnEdit :=
    function (const AContact: TContact): TContactState
    begin
      Hide();
      try
        Result := TViewContactEdit.Execute(Self, AContact, TWorkMode.Edit);
        Show();
      finally
        Close();
      end;
    end;
end;

end.

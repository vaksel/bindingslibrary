unit View.ContactEdit;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Types,
  System.UITypes,
  System.Rtti,
  System.ImageList,
  System.Bindings.Outputs,
  FMX.Types,
  FMX.Graphics,
  FMX.Controls,
  FMX.Forms,
  FMX.Dialogs,
  FMX.StdCtrls,
  FMX.Layouts,
  FMX.Controls.Presentation,
  FMX.ScrollBox,
  FMX.Memo,
  FMX.Ani,
  FMX.Objects,
  FMX.DateTimeCtrls,
  FMX.Edit,
  FMX.ImgList,
  Fmx.Bind.DBEngExt,
  Fmx.Bind.Editors,
  Data.Bind.Components,
  Data.Bind.EngExt,
  View.CustomForm,
  Bindings.Expressions.Components,
  Bindings.Expressions.Scope,
  Bindings.Notifications.Handler.FMX,
  Model.Photo,
  Model.Contact,
  ViewModel.Contact;

type
  TViewContactEdit = class(TViewCustomForm)
    il1: TImageList;
    sbxCenter: TVertScrollBox;
    laytCenter: TRectangle;
    laytMobilePhone: TLayout;
    laytMobilePhoneHeader: TLayout;
    imgMobilePhoneHeaderIcon: TGlyph;
    lblMobilePhoneHeaderCaption: TLabel;
    lnMobilePhoneHeaderSeparator: TLine;
    edtMobilePhone: TEdit;
    laytBirthday: TLayout;
    laytBirthdayHeader: TLayout;
    imgBirthdayHeaderIcon: TGlyph;
    lblBirthdayHeaderCaption: TLabel;
    lnBirthdayHeaderSeparator: TLine;
    edtBirthday: TDateEdit;
    laytWork: TLayout;
    laytWorkHeader: TLayout;
    imgWorkHeaderIcon: TGlyph;
    lblWorkHeaderCaption: TLabel;
    lnWorkHeaderSeparator: TLine;
    edtWork: TEdit;
    laytWorkPhone: TLayout;
    laytWorkPhoneHeader: TLayout;
    imgWorkPhoneHeaderIcon: TGlyph;
    lblWorkPhoneHeaderCaption: TLabel;
    lnWorkPhoneHeaderSeparator: TLine;
    edtWorkPhone: TEdit;
    laytNote: TLayout;
    laytNoteHeader: TLayout;
    imgNoteHeaderIcon: TGlyph;
    lblNoteHeaderCaption: TLabel;
    lnNoteHeaderSeparator: TLine;
    laytHeader: TRectangle;
    imgPhoto: TCircle;
    aniDelete: TFloatAnimation;
    btnMore: TLayout;
    imgMore: TGlyph;
    aniMore: TFloatAnimation;
    laytName: TLayout;
    laytFooter: TLayout;
    btnCancel: TButton;
    edtNote: TMemo;
    edtFirstName: TEdit;
    edtLastName: TEdit;
    BindPhoto: TBindExpression;
    BindPhoneMobile: TBindingControl;
    BindWorkPhone: TBindingControl;
    BindBirthday: TBindingControl;
    BindNote: TBindingControl;
    BindFirstName: TBindingControl;
    BindLastName: TBindingControl;
    BindingWork: TBindingControl;
    btnSave: TButton;
    errMobilePhone: TImage;
    BindPhoneMobileError: TBindExprItems;
    errWorkPhone: TImage;
    BindWorkPhoneError: TBindExprItems;
    dlgOpenPhoto: TOpenDialog;
    BindingCmdChangePhoto: TBindingCommand;
    BindCmdDelete: TBindingCommand;
    btnDelete: TSpeedButton;
  protected
    procedure DoInitViewModel(); override;
  public
    class function Execute(const AOwner: TForm; const AContact: TContact;
      const AEditMode: TWorkMode): TContactState;
  end;

implementation

{$R *.fmx}

{ TViewContactEdit }

class function TViewContactEdit.Execute(const AOwner: TForm; const AContact: TContact;
  const AEditMode: TWorkMode): TContactState;
var
  LView      : TViewContactEdit;
  LViewModel : TViewModelContact;
begin
  LViewModel := TViewModelContact.CreateEx(AContact, AEditMode);
  try
    LView := Self.Create(AOwner);
    try
      LView.SetViewModel(LViewModel, False);
      LView.ShowModal();
    finally
      FreeAndNil(LView);
    end;
    Result := LViewModel.State;
  finally
    FreeAndNil(LViewModel);
  end;
end;

procedure TViewContactEdit.DoInitViewModel();
begin
  inherited;
  ViewModelAs<TViewModelContact>.OnUpdatePhoto :=
    function (const APhoto: TPhoto): Boolean
    begin
      Result := dlgOpenPhoto.Execute;
      if (Result) then begin
        APhoto.LoadFromFile(dlgOpenPhoto.FileName);
      end;
    end;
end;

end.

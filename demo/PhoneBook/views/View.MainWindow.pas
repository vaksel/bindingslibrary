unit View.MainWindow;

interface

uses
  System.SysUtils,
  System.Types,
  System.UITypes,
  System.Classes,
  System.Rtti,
  System.Bindings.Outputs,
  Fmx.Bind.DBEngExt,
  Fmx.Bind.Editors,
  FMX.ListView.Types,
  FMX.ListView.Appearances,
  FMX.ListView.Adapters.Base,
  FMX.ListView,
  FMX.Controls,
  FMX.Controls.Presentation,
  FMX.Edit,
  FMX.Objects,
  FMX.Types,
  FMX.Layouts,
  FMX.StdCtrls,
  Data.Bind.Components,
  Data.Bind.EngExt,
  Bindings.Expressions.Components,
  Bindings.Expressions.Scope,
  Bindings.Notifications.Handler.FMX,
  Model.Contact,
  Model.Contacts,
  ViewModel.ContactList,
  ViewModel.Contact,
  View.CustomForm,
  View.ContactEdit,
  View.ContactView,
  Helper.ContactsSample;

type
  TViewMain = class(TViewCustomForm)
    Rectangle1: TRectangle;
    RoundRect1: TRoundRect;
    edt1: TEdit;
    lvContacts: TListView;
    Rectangle2: TRectangle;
    BindFillContacts: TBindingList;
    BindFilter: TBindingControl;
    BindContactsItemIndex: TBindingControl;
    pnlFooter: TGridPanelLayout;
    btnViewContact: TSpeedButton;
    btnDeleteContact: TSpeedButton;
    BindCmdAddContact: TBindingCommand;
    BindCmdViewContact: TBindingCommand;
    BindCmdDeleteContact: TBindingCommand;
    btnAddContact: TSpeedButton;
    BindViewContact: TBindingCommand;
    procedure lvContactsItemClickEx(const Sender: TObject; ItemIndex: Integer;
      const LocalClickPos: TPointF; const ItemObject: TListItemDrawable);
  strict private
  var
    FContacts : TContacts;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy(); override;
  end;

var
  ViewMain: TViewMain;

implementation

{$R *.fmx}

constructor TViewMain.Create(AOwner: TComponent);
var
  LViewModel : TViewModelContactList;
begin
  inherited;
  FContacts := TContactsSample.CreateSampleData(100);

  LViewModel := TViewModelContactList.CreateEx(FContacts);
  LViewModel.OnContactView :=
    function (const AContact: TContact): TContactState
    begin
      Result := TViewContactView.Execute(Self, AContact, TWorkMode.View);
    end;
  LViewModel.OnContactAdd :=
    function (const AContact: TContact): TContactState
    begin
      Result := TViewContactEdit.Execute(Self, AContact, TWorkMode.Create);
    end;
  SetViewModel(LViewModel, True);
end;

destructor TViewMain.Destroy();
begin
  FreeAndNil(FContacts);
  inherited;
end;

procedure TViewMain.lvContactsItemClickEx(const Sender: TObject; ItemIndex: Integer;
  const LocalClickPos: TPointF; const ItemObject: TListItemDrawable);
begin
  if (ItemObject is TListItemAccessory) then begin
    ViewModelAs<TViewModelContactList>.CmdViewContact.Data.Run();
  end;
end;

end.

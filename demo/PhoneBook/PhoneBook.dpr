program PhoneBook;

uses
  System.StartUpCopy,
  FMX.Forms,
  View.MainWindow in 'views\View.MainWindow.pas' {ViewMain},
  Model.Contact in 'models\Model.Contact.pas',
  Model.Contacts in 'models\Model.Contacts.pas',
  ViewModel.Contact in 'viewmodels\ViewModel.Contact.pas',
  View.ContactEdit in 'views\View.ContactEdit.pas' {ViewContactEdit},
  ViewModel.ContactList in 'viewmodels\ViewModel.ContactList.pas',
  View.ContactView in 'views\View.ContactView.pas' {ViewContactView},
  Helper.ContactsSample in 'helpers\Helper.ContactsSample.pas',
  Model.Photo in 'models\Model.Photo.pas',
  View.CustomForm in '..\share\views\FMX\View.CustomForm.pas' {ViewCustomForm},
  ViewModel.Custom in '..\share\viewmodels\ViewModel.Custom.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TViewMain, ViewMain);
  Application.Run;
end.

unit Helper.ContactsSample;

interface

uses
  System.SysUtils,
  System.UITypes,
  Model.Contact,
  Model.Contacts,
  FMX.Graphics,
  FMX.Types;

type
  TContactsSample = record
  strict private
    class procedure CreateAvatarFor(const AContact: TContact); static;
    class function CreatePhoneNumber(): String; static;
  public
    class function CreateSampleData(const ACount: Int32): TContacts; static;
    class function CreateSampleDataForTest(const ACount: Int32): TContacts; static;
  end;

implementation

{ TContactsSample }

class procedure TContactsSample.CreateAvatarFor(const AContact: TContact);
var
  LBitmap : TBitmap;
  LText   : String;
begin
  LBitmap := TBitmap.Create(256, 256);
  try
    LBitmap.Canvas.BeginScene();
    try
      LBitmap.Canvas.ClearRect(LBitmap.BoundsF, TAlphaColorRec.Skyblue);
      LText := AContact.FirstName.Substring(0, 1) + AContact.LastName.Substring(0, 1);
      LBitmap.Canvas.Fill.Color := TAlphaColorRec.White;
      LBitmap.Canvas.Font.Size := 128;
      LBitmap.Canvas.Font.Style := [TFontStyle.fsBold];
      LBitmap.Canvas.FillText(LBitmap.BoundsF, LText, False, 1, [], TTextAlign.Center, TTextAlign.Center);
    finally
      LBitmap.Canvas.EndScene();
    end;
    AContact.Photo := LBitmap;
  finally
    FreeAndNil(LBitmap);
  end;
end;

class function TContactsSample.CreatePhoneNumber(): String;
var
  I: Integer;
begin
  SetLength(Result, 10);
  for I := 1 to Length(Result) do begin
    Result[I] := Random(10).ToString()[1];
  end;
end;

class function TContactsSample.CreateSampleData(const ACount: Int32): TContacts;
{$REGION 'names'}
const
  FIRST_NAMES : array [0..17] of String = (
    'John',
    'Robert',
    'Michael',
    'William',
    'David',
    'Richard',
    'Joseph',
    'Thomas',
    'Charles',
    'Patricia',
    'Jennifer',
    'Linda',
    'Elizabeth',
    'Barbara',
    'Susan',
    'Jessica',
    'Sarah',
    'Margaret'
  );
  LAST_NAMES : array [0..17] of String = (
    'Smith',
    'Johnson',
    'Williams',
    'Jones',
    'Brown',
    'Davis',
    'Miller',
    'Wilson',
    'Moore',
    'Taylor',
    'Anderson',
    'Thomas',
    'Jackson',
    'White',
    'Harris',
    'Martin',
    'Thompson',
    'Garcia');
{$ENDREGION 'names'}
var
  LContact : TContact;
  I        : Int32;
begin
  Result := TContacts.Create();
  for I := 0 to ACount - 1 do begin
    LContact := TContact.Create();
    LContact.FirstName := FIRST_NAMES[Random(18)];
    LContact.LastName := LAST_NAMES[Random(18)];
    LContact.PhoneMobile := CreatePhoneNumber();
    LContact.PhoneWork := CreatePhoneNumber();
    LContact.Work := 'Doctor';
    LContact.Note := 'Some long'#13#10'long note';
    LContact.Birthday := EncodeDate(1950 + Random(60), Random(12) + 1, Random(27) + 1);
    CreateAvatarFor(LContact);
    Result.Add(LContact);
  end;
end;

class function TContactsSample.CreateSampleDataForTest(const ACount: Int32): TContacts;
var
  LContact : TContact;
  I        : Int32;
begin
  Result := TContacts.Create();
  for I := 0 to ACount - 1 do begin
    LContact := TContact.Create();
    LContact.FirstName := 'James';
    LContact.LastName := 'Bond';
    LContact.PhoneMobile := '1111111111';
    LContact.PhoneWork := '2222222222';
    LContact.Work := 'Spy';
    LContact.Note := 'Note';
    LContact.Birthday := EncodeDate(1950, 1, 1);
    Result.Add(LContact);
  end;
end;

end.
